///////////////////////////////////////////////////////////////////////////////////////////////////
// Genetic Algorithms
///////////////////////////////////////////////////////////////////////////////////////////////////

// Inclusión de bibliotecas de funciones
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <malloc.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
// Generador de números pseudoaleatorios
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AG_RANDOM
#define _AG_RANDOM

#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

// Rand genera un numero real pseudoaleatorio entre 0 y 1, excluyendo el 1
#define Rand()  (( Seed =(Seed * PRIME) & MASK) * SCALE )

// Randint genera un numero entero entre low y high, ambos incluidos
#define Randint(low,high) (int)(low + (high - (low) + 1) * Rand()) 

// Randfloat genera un numero real entre low y high, incluido low y no incluido high
#define Randfloat(low,high) ((low + (high - (low)) * Rand() ))

#endif
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones auxiliares
///////////////////////////////////////////////////////////////////////////////////////////////////

// Función que devuelve el máximo entre dos elementos
float Max(float uno, float dos)
{
	return (uno > dos)? uno: dos;
}


// Función que devuelve el mínimo entre dos elementos
float Min(float uno, float dos)
{
	return (uno < dos)? uno: dos;
}


// Sign
float Sign(float a)
{
	return (a < 0)? (float)(-1): (float)(1);
}


// Error
void Error(char *s)
{
	fprintf(stderr, "Error: %s.", s);
	exit(1);
}
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
// Algoritmos Evolutivos
///////////////////////////////////////////////////////////////////////////////////////////////////


// Definición de parámetros
#define ITERACIONES 15000	//
#define GENERACIONES 300 //
#define POBLACION 50 //
#define MUTACION 0.4
#define CRUCE 0.2
#define RD 0.5
#define FD 0.01
#define NBITS 10 //
#define K_TORNEO 4






// Función de Fitness. Parametrización Elíptica Pura
// Cálculo de la función objetivo BEM_Elipses
float Objetivo_BEM_Elipse(BEM_Elipses X)
{

}



// Creación de una Generacion_BEM_Elipse de tamaño N
Generacion_BEM_Elipse Crear_Generacion_BEM_Elipse(int N)
{

}



// Destrucción de un Generacion_BEM_Elipse
void Destruir_Generacion_BEM_Elipse(Generacion_BEM_Elipse X)
{
	// Liberar memoria
	free(X->Poblacion);
	free(X->Ps);
	free(X);
}



// Inicializar Generacion_BEM_Elipse
void InicGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, BEM_Placa 
							   Problema, int * Seed1)
{
	BEM_Elipses Nuevo;
	int i, Seed=*Seed1;

	// Asignar aleatoriamente todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(Problema);
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);
		G->Poblacion[i]=Nuevo;
	}

	*Seed1=Seed;
}



// Inicializar Generacion_BEM_Elipse
void ReiniciarGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, int * Seed1)
{
	BEM_Elipses Nuevo, Best;
	int i, pos, Seed=*Seed1;
	float Maximo=-FLT_MAX;

	// Hallar el mejor cromosoma
	for(i=0; i<G->Tam; i++)
	{
		if(G->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo a la nueva generacion y exterminar el resto
	Best=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
	Copia_BEM_Elipses(G->Poblacion[pos], Best);
	Exterminar_Generacion_BEM_Elipse(G);
	G->Poblacion[0]=Best;

	// Asignar aleatoriamente los demás individuos
	for(i=1; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);
		G->Poblacion[i]=Nuevo;
	}

	*Seed1=Seed;
}



// Exterminar Generacion_BEM_Elipse
void Exterminar_Generacion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;

	for(i=0; i<G->Tam; i++)
		Destruir_BEM_Elipses(G->Poblacion[i]);
}




// Asignar probabilidades de selección
void AsignarPs_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;
	float Sumatoria=(float)0.0, minimo=0;

	// Calcular minimo
	for(i=0; i<G->Tam; i++)
	{
		if(G->Poblacion[i]->CostoActual<minimo)
			minimo=G->Poblacion[i]->CostoActual;
	}

	// Calcular sumatoria de costos
	for(i=0; i<G->Tam; i++)
		Sumatoria+=G->Poblacion[i]->CostoActual-minimo;

	// Caso especial
	if(Sumatoria==0)
	{
		G->Ps[0]=(float)1/(float)G->Tam;

		for(i=1; i<G->Tam; i++)
			G->Ps[i]=1/(float)G->Tam+G->Ps[i-1];
	}
	else
	{
		// Asignar valores de función de densidad
		G->Ps[0]=(G->Poblacion[0]->CostoActual-minimo)/Sumatoria;

		for(i=1; i<G->Tam; i++)
			G->Ps[i]=(G->Poblacion[i]->CostoActual-minimo)/Sumatoria+G->Ps[i-1];
	}
}





// Selección de Baker
void SeleccionBaker_BEM_Elipse(Generacion_BEM_Elipse G, 
							   int *Seed1, int M)
{
	float valor;
	int Seed=*Seed1;
	int i, pos;

	// Marcar todos los cromosomas como no seleccionados
	for(i=0; i<G->Tam; i++)
		G->Selec[i]=0;

	// Girar la ruleta
	valor=(float)Randfloat(0, 1);

	pos=0;
	while(G->Ps[pos]<valor)
		pos++;
	
	// Marcar como seleccionado
	G->Selec[pos]=1;

	// Repetir M-1 veces
	for(i=1; i<M; i++)
	{
		// Salto de tamaño 1/M
		valor+=(float)1/(float)M;
		
		if(valor>(float)1.0)
			valor-=(float)1.0;
		
		pos=0;
		while(G->Ps[pos]<valor)
			pos++;
		
		// Marcar como seleccionado
		G->Selec[pos]++;
	}

	*Seed1=Seed;
}





// Evaluar Generacion_BEM_Elipse
void EvGeneracion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;

	// Para todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		// Evaluar la función objetivo
		G->Poblacion[i]->CostoActual=Objetivo_BEM_Elipse(G->Poblacion[i]);
	}
}







class GA
{
	private:
		char* m_nombre;
		char* m_departamento;
		char* m_posicion;
		long m_salario;
BEM_Placa Problema, char * f_graficos,
								   int Seed
								   
	public:
		int generaciones = 50;
		
		// Algoritmo Genético Generacional
		void GGA()
		{
			Generacion_BEM_Elipse G1, G2;
			BEM_Elipses Mejor;
			int i, j, iteraciones, pos;
			float Maximo=-FLT_MAX;

			// Crear BEM_Elipses solución
			Mejor=Crear_BEM_Elipses(Problema);


			//
			// AGG
			//

			// Crear ecosistema
			G1 = Crear_Generacion_BEM_Elipse(POBLACION);
			G2 = Crear_Generacion_BEM_Elipse(POBLACION);

			// Crear primera generación
			InicGeneracion_BEM_Elipse(G1, Problema, &Seed);

			// Evaluar generación
			EvGeneracion_BEM_Elipse(G1);


			// Generaciones
			for(i = 0; i < this.generaciones; i++)
			{
				// Verbose
				if(!(i%8))
				{		
					// Buscar el mejor cromosoma
					Maximo=-FLT_MAX;
					for(j=0; j<G1->Tam; j++)
					{
						if(G1->Poblacion[j]->CostoActual > Maximo)
						{
							Maximo=G1->Poblacion[j]->CostoActual;
							pos=j;
						}
					}

					printf("%d %f\n", i, G1->Poblacion[pos]->CostoActual);
					Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);
				}

				// Selección de cromosomas
				AsignarPs_BEM_Elipse(G1);
				SeleccionBaker_BEM_Elipse(G1, &Seed, POBLACION);

				// Siguiente generación
				for(pos=0, j=0; j<G1->Tam; j++)
				{
					while(G1->Selec[j])
					{
						G2->Poblacion[pos]=Crear_BEM_Elipses(Problema);
						Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
						pos++;
						G1->Selec[j]--;
					}
				}
				
				// Cruce
				AGG_Cruce_BEM_Elipse(G2, &Seed);

				// Mutación
				for(j=0; j<G2->Tam; j++)
				{
					if(Randfloat(0, 1)<MUTACION)
					{
						AG_Mutacion_BEM_Elipse(G2->Poblacion[j], &Seed);
						AG_Mutacion2_BEM_Elipse(G2->Poblacion[j], &Seed);
					}
				}

				// Evaluar
				EvGeneracion_BEM_Elipse(G2);

				// Envejecimiento de la población con elitismo
				SustGeneracion_BEM_Elipse(G1, G2);
				
				/*// Exterminar generación anterior
				Exterminar_Generacion_BEM_Elipse(G1);
				// Envejecimiento de la población
				for(j=0; j<G2->Tam; j++) G1->Poblacion[j]=G2->Poblacion[j];*/
			}


			// Buscar el mejor cromosoma
			Maximo=-FLT_MAX;
			for(i=0; i<G1->Tam; i++)
			{
				if(G1->Poblacion[i]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[i]->CostoActual;
					pos=i;
				}
			}

			// Copiarlo y exterminar al resto
			Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
			Exterminar_Generacion_BEM_Elipse(G1);

			// Liberar memoria
			Destruir_Generacion_BEM_Elipse(G1);
			Destruir_Generacion_BEM_Elipse(G2);

			return Mejor;
		}

		
		
		// Cruce generacional
		void cruceGeneracional()
		{
			BEM_Elipses aux, Hijo1, Hijo2;
			int Seed=*Seed1;
			int i, pos, iteraciones;

			iteraciones=(int)(G->Tam*CRUCE);

			// Barajar
			for(i=0; i<G->Tam; i++)
			{
				pos=Randint(0, G->Tam-1);
				aux=G->Poblacion[i];
				G->Poblacion[i]=G->Poblacion[pos];
				G->Poblacion[pos]=aux;
			}
				
			// Cruzar
			for(i=0; i<iteraciones; i+=2)
			{
				Hijo1=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
				Hijo2=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
				
				AG_Cruce_BEM_Elipse(G->Poblacion[i], G->Poblacion[i+1], 
				Hijo1, Hijo2, &Seed);
				
				Destruir_BEM_Elipses(G->Poblacion[i]);
				Destruir_BEM_Elipses(G->Poblacion[i+1]);
				G->Poblacion[i]=Hijo1;
				G->Poblacion[i+1]=Hijo2;
			}

			*Seed1=Seed;
		}
		
		
		
		//
		// Operadores Genéticos
		//


		// Operador Cruce
		void cruce(cromosome father, cromosome mother, cromosome child1, cromosome child2)
		{
			int Seed=*Seed1, valor, i, j;

			// Copiar Padres en los Hijos
			child1 = father;
			child2 = mother;

			// Cruce simple

			// Elegir punto de cruce
			valor = Randint(0, Padre->Tam-1);

			// Cruzar
			// Para todas las elipses
			for(i = valor; i < Padre->Tam; i++)
			{
				// Para todos los genes
				for(j = 0; j < Padre->Elipse[0].NParams; j++)
				{
					Hijo1->Elipse[i].V[j]=Madre->Elipse[i].V[j];
					Hijo2->Elipse[i].V[j]=Padre->Elipse[i].V[j];
				}
			}

			*Seed1=Seed;
		}
		


		// Operador Mutación
		void mutacion(cromosome X)
		{
			int Seed=*Seed1, i, j;

			// Para todas las elipses
			for(i=0; i<Antiguo->Tam; i++)
			{
				// Para todos los parámetros
				for(j=0; j<Antiguo->Elipse[0].NParams; j++)
				{
					// Ratio de desplazamiento
					if(Randfloat(0,1) < RD)
					{
						// Incremento
						if(Randfloat(0,1) < 0.5)
							Antiguo->Elipse[i].V[j]+=(float)FD*(Antiguo->Lim[2*j+1] 
								- Antiguo->Lim[2*j]);
						// Disminución
						else
							Antiguo->Elipse[i].V[j]-=(float)FD*(Antiguo->Lim[2*j+1]
								- Antiguo->Lim[2*j]);
					
						// Ajuste
						if(Antiguo->Elipse[i].V[j] < Antiguo->Lim[2*j])
							Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j];
						if(Antiguo->Elipse[i].V[j] > Antiguo->Lim[2*j+1])
							Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j+1];
					}
				}
			}

			*Seed1=Seed;
		}
		
		
		


		// Realizar una sustitución de cromosomas con elitismo
		void generationChange()
		{
			int i, j, *Pos, Maxpos;
			float Max;

			// Reservar memoria
			Pos=(int*)malloc(sizeof(int)*G1->Tam*2);
			if(Pos==NULL)
				Error("sin memoria para Pos");

			// Inicializar cromosomas elegidos
			for(i=0; i<G1->Tam*2; i++)
				Pos[i]=0;

			// Elegir los N mejores
			for(i=0; i<G1->Tam; i++)
			{
				Max=-FLT_MAX;
				Maxpos=-1;

				for(j=0; j<G1->Tam*2; j++)
				{
					// Si no está seleccionado
					if(!Pos[j])
					{
						// Buscar en generación anterior
						if(j<G1->Tam)
						{
							if(G1->Poblacion[j]->CostoActual>Max)
							{
								Max=G1->Poblacion[j]->CostoActual;
								Maxpos=j;
							}
						}
						// Buscar en generación nueva
						else
						{
							if(G2->Poblacion[j-G1->Tam]->CostoActual>Max)
							{
								Max=G2->Poblacion[j-G1->Tam]->CostoActual;
								Maxpos=j;
							}
						}
					}
				}

				// Seleccionar
				Pos[Maxpos]++;
			}

			// Eliminar los no elegidos
			for(i=0; i<G1->Tam*2; i++)
			{
				if(!Pos[i])
				{
					if(i<G1->Tam)
						Destruir_BEM_Elipses(G1->Poblacion[i]);
					else
						Destruir_BEM_Elipses(G2->Poblacion[i-G1->Tam]);
				}
			}

			// Salvar los seleccionados
			for(i = 0, j = 0; i < G1->Tam; i++)
			{
				if(Pos[i])
				{
					if(i<G1->Tam)
					{
						G1->Poblacion[j]=G1->Poblacion[i];
					}
					else
					{
						G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
					}

					j++;
				}
			}

			// Liberar memoria
			free(Pos);
		}
};






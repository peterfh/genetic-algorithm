
		//
		// Fichero de Cabecera Genetic_BEM
		// Declaración de Funciones
		//


// Sirve de puente entre los AG y las funciones BEM

// Inclusión de bibliotecas de funciones y TDAs
#include "../BEM/TDA_Contorno.h"
#include "BEM_Elipse.h"


#ifndef _GENETIC_BEM
#define _GENETIC_BEM


// Función de Fitness. Parametrización Elíptica Pura
// Cálculo de la función objetivo BEM_Elipses
float Objetivo_BEM_Elipse(BEM_Elipses X);

// Param_Elipses
// Parametrización pura por elipses
void Param_Elipses(TElipse **elipses, BEM_Elipses X);


#endif
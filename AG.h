
			/********************************/
			/*   AGs. Problema BEM_Elipse   */
			/********************************/


// Inclusi�n de bibliotecas
#include "BEM_Elipse.h"


#ifndef _AG_BEM_Elipse
#define _AG_BEM_Elipse



// Definici�n del TDA Generacion_BEM_Elipse
typedef struct
{
	BEM_Elipses * Poblacion;
	float * Ps;
	int * Selec;
	int Tam;
} TDA_Generacion_BEM_Elipse;

typedef TDA_Generacion_BEM_Elipse * Generacion_BEM_Elipse;


// Creaci�n de una Generacion_BEM_Elipse de tama�o N
Generacion_BEM_Elipse Crear_Generacion_BEM_Elipse(int N);

// Destrucci�n de un Generacion_BEM_Elipse
void Destruir_Generacion_BEM_Elipse(Generacion_BEM_Elipse X);

// Inicializar Generacion_BEM_Elipse
void Inic_Generacion_BEM_Elipse(Generacion_BEM_Elipse G, BEM_Placa
								 Problema, int * Seed1);

// Inicializar Generacion_BEM_Elipse con Longitud Variable
void InicGeneracion_BEM_Elipse_LV(Generacion_BEM_Elipse G, BEM_Placa 
							   Problema, int * Seed1);

// Inicializar Generacion_BEM_Elipse
void ReiniciarGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, int * Seed1);

// Exterminar Generacion_BEM_Elipse
void Exterminar_Generacion_BEM_Elipse(Generacion_BEM_Elipse G);

// Estad�sticas Generacion_BEM_Elipse
void Estadisticas_Generacion_BEM_Elipse(Generacion_BEM_Elipse G, 
									  float * Media, float * Desviacion);

// Asignar probabilidades de selecci�n
void AsignarPs_BEM_Elipse(Generacion_BEM_Elipse G);

// Selecci�n de Baker
void SeleccionBaker_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1, int M);

// Selecci�n por torneo
void SeleccionTorneo_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1);

// Evaluar Generacion_BEM_Elipse
void EvGeneracion_BEM_Elipse(Generacion_BEM_Elipse G);

// Evaluar Generacion_BEM_Elipse con Fitness Sharing
void Ev2Generacion_BEM_Elipse(Generacion_BEM_Elipse G);


// Algoritmo Gen�tico Generacional para el problema BEM_Elipse
BEM_Elipses AGG_BEM_Elipse(BEM_Placa Problema, char * f_graficos,
						   int Seed);

// Algoritmo Gen�tico Generacional Longitud Variable para el problema BEM_Elipse
BEM_Elipses AGGLV_BEM_Elipse(BEM_Placa Problema, char * f_graficos,
						   int Seed);

// Algoritmo Gen�tico CHC para el problema BEM_Elipse
BEM_Elipses AGCHC_BEM_Elipse(BEM_Placa Problema, char * f_graficos,
							 int Seed);

// Estrategia de Evoluci�n 1,1 aplicada sobre el problema utilizando elipses
BEM_Elipses Ee1_BEM_Elipse(BEM_Placa Problema, char * f_graficos, 
						   int Seed);

// Estrategia de Evoluci�n 15,105 aplicada sobre el problema utilizando elipses
BEM_Elipses Ee2_BEM_Elipse(BEM_Placa Problema, char * f_graficos, 
						   int Seed);


// Operador Cruce
void AG_Cruce_BEM_Elipse(BEM_Elipses Padre, BEM_Elipses Madre, 
					BEM_Elipses Hijo1, BEM_Elipses Hijo2, int * Seed1);

// Operador Mutaci�n
void AG_Mutacion_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1);

// Operador Mutaci�n de Longitud de cromosoma
void AG_MutaLongitud_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1);

// Operador Mutaci�n Aleatorio Simple
void AG_Mutacion2_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1);

// Operador Mutaci�n para Estrategias de Evoluci�n
void Ee_Mutacion_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1, float varianza);

// Realizar una sustituci�n de cromosomas con elitismo
void SustGeneracion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2);

// Realizar una sustituci�n de cromosomas con elitismo variable
void SustGeneracion2_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2);

// Cruce generacional en Generacion_BEM_Elipse
void AGG_Cruce_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1);

// Cruce para Estrategias de Evoluci�n
void Ee_Cruce_BEM_Elipse(Generacion_BEM_Elipse G1, Generacion_BEM_Elipse G2, int *Seed1);

// Cruce generacional en Generacion_BEM_Elipse
void AGCHC_Cruce_BEM_Elipse(Generacion_BEM_Elipse G, int * Umbral, 
							int *Seed1);

// Realizar una selecci�n de padres para la siguiente poblaci�n
void Seleccion_Generacion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2);

// Realizar una selecci�n de padres en Estrategias de Evoluci�n
void Ee_Seleccion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2);

// Distancia entre BEM_Elipses
int Distancia_BEM_Elipses(BEM_Elipses C1, BEM_Elipses C2);


#endif

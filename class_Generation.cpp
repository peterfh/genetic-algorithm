///////////////////////////////////////////////////////////////////////////////////////////////////
// Genetic Algorithms
///////////////////////////////////////////////////////////////////////////////////////////////////

// Inclusión de bibliotecas de funciones
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <malloc.h>

#include "class_Generation.hxx"

///////////////////////////////////////////////////////////////////////////////////////////////////
// Class Generation
///////////////////////////////////////////////////////////////////////////////////////////////////

class Generation
{
	public:
		Chromosome individuals[GENERACION];
		int size = GENERACION;

		Generation()
		{
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////////
		// Inicializar
		////////////////////////////////////////////////////////////////////////////////////////////
		void initialize()
		{
			// Asignar aleatoriamente todos los individuos
			for(int i = 0; i < this->size; i++)
			{
				this->individuals[i]->random();
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////////
		// Reiniciar
		////////////////////////////////////////////////////////////////////////////////////////////
		void reset()
		{
			int i, pos;
			float Max = -FLT_MAX;

			// Hallar el mejor cromosoma
			for(i = 0; i < this->size; i++)
			{
				if(this->individuals[i]->value > Max)
				{
					Max = this->individuals[i]->value;
					pos = i;
				}
			}

			// Copiarlo a la nueva generacion y exterminar el resto
			this->individuals[0] = this->individuals[i];

			// Asignar aleatoriamente los demás individuos
			for(i = 1; i < this->size; i++)
			{
				this->individuals[i]->random();
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////////
		// Evaluar
		////////////////////////////////////////////////////////////////////////////////////////////
		void evaluate()
		{
			// Para todos los individuos
			for(int i = 0; i < this->size; i++)
			{
				this->individuals[i]->eval();
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////////
		// Asignar probabilidades de selección
		////////////////////////////////////////////////////////////////////////////////////////////
		void selectionProbabilities()
		{
			int i;
			float Sumatoria=(float)0.0, Min = 0;

			// Calcular minimo
			for(i = 0; i < this->size; i++)
			{
				if(this->individuals[i]->value < Min)
				{
					Min = this->individuals[i]->value;
				}
			}

			// Calcular sumatoria de costos
			for(i = 0; i < this->size; i++)
			{
				Sumatoria += this->individuals[i]->value - Min;
			}

			// Caso especial
			if(Sumatoria == 0)
			{
				this->individuals[0]->selectionProbability = (float)1 / (float)this->size;

				for(i = 1; i < this->size; i++)
				{
					this->individuals[i]->selectionProbability = this->individuals[0]->selectionProbability + this->individuals[i - 1]->selectionProbability;
				}
			}
			else
			{
				float invSumatoria = (float)1 / Sumatoria;
				
				// Asignar valores de función de densidad
				this->individuals[0]->selectionProbability = (this->individuals[0]->value - Min) * invSumatoria;

				for(i = 1; i < this->size; i++)
				{
					this->individuals[i]->selectionProbability = (this->individuals[i]->value - Min) * invSumatoria + this->individuals[i - 1]->selectionProbability;
				}
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////////
		// Selección de Baker
		////////////////////////////////////////////////////////////////////////////////////////////
		void selectBaker(int M)
		{
			float valor;
			int i, pos;

			// Marcar todos los cromosomas como no seleccionados
			for(i = 0; i < this->size; i++)
			{
				this->individuals[i]->selected = false;
			}

			// Girar la ruleta
			valor = (float)Randfloat(0, 1);

			pos = 0;
			while(this->individuals[pos] < valor)
			{
				pos++;
			}
			
			// Marcar como seleccionado
			this->individuals[pos]->selected = true;

			// Repetir M-1 veces
			for(i = 1; i < M; i++)
			{
				// Salto de tamaño 1/M
				valor += (float)1 / (float)M;
				
				if(valor > (float)1.0)
				{
					valor -= (float)1.0;
				}
				
				pos = 0;
				while(ths->individuals[pos] < valor)
				{
					pos++;
				}
				
				// Marcar como seleccionado
				this->individuals[pos]->selected = true;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////////
		// Cruce generacional
		////////////////////////////////////////////////////////////////////////////////////////////
		void cross()
		{
			int i, pos, cruces;

			cruces = (int)(this->size * CRUCE);

			// Barajar
			for(i = 0; i < this->size; i++)
			{
				pos = Randint(0, this->size - 1);

				// Swap (i, pos)
				aux = this->individuals[i];
				this->individuals[i] = this->individuals[pos];
				this->individuals[pos] = aux;
			}
				
			// Cruzar
			for(i = 0; i < cruces; i += 2)
			{
				Child1 = new Chromosome;
				Child2 = new Chromosome;
				
				this->individuals[i]->cross(this->individuals[i + 1], Child1, Child2);
				
				delete(this->individuals[i]);
				delete(this->individuals[i + 1]);

				this->individuals[i] = Child1;
				this->individuals[i + 1] = Child2;
			}
		}
}

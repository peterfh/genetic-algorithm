///////////////////////////////////////////////////////////////////////////////////////////////////
// Genetic Algorithms
///////////////////////////////////////////////////////////////////////////////////////////////////

// Inclusión de bibliotecas de funciones
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <malloc.h>

#include "class_Chromosome.hxx"


///////////////////////////////////////////////////////////////////////////////////////////////////
// Class Chromosome
///////////////////////////////////////////////////////////////////////////////////////////////////

int Seed;

class Chromosome
{
	public:
		float value;
		float selectionProbability;
		bool selected;
		
		// Función de Fitness. Cálculo de la función objetivo
		float eval()
		{
			this->value = 0;
		}


		////////////////////////////////
		// Operadores Genéticos
		////////////////////////////////

		// Operador Cruce
		void cross(Chromosome mother, Chromosome child1, Chromosome child2)
		{
			int valor, i, j;

			// Copiar Padres en los Hijos
			child1 = new Chromosome();
			child2 = new Chromosome();

			// Cruce simple

			// Elegir punto de cruce
			valor = Randint(0, Padre->Tam-1);

			// Cruzar
			// Para todos los genes
			for(j = 0; j < Padre->Elipse[0].NParams; j++)
			{
				Hijo1->Elipse[i].V[j] = Madre->Elipse[i].V[j];
				Hijo2->Elipse[i].V[j] = Padre->Elipse[i].V[j];
			}
		}


		// Operador Mutación
		void mutation()
		{
			int i, j;

			// Para todos los parámetros
			for(j=0; j<Antiguo->Elipse[0].NParams; j++)
			{
				// Ratio de desplazamiento
				if(Randfloat(0,1) < RD)
				{
					// Incremento
					if(Randfloat(0,1) < 0.5)
						Antiguo->Elipse[i].V[j]+=(float)FD*(Antiguo->Lim[2*j+1] - Antiguo->Lim[2*j]);
					// Disminución
					else
						Antiguo->Elipse[i].V[j]-=(float)FD*(Antiguo->Lim[2*j+1]	- Antiguo->Lim[2*j]);
					
					// Ajuste
					if(Antiguo->Elipse[i].V[j] < Antiguo->Lim[2*j])
						Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j];
					if(Antiguo->Elipse[i].V[j] > Antiguo->Lim[2*j+1])
						Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j+1];
				}
			}
		}
}



///////////////////////////////////////////////////////////////////////////////////////////////////
// Class Generation
///////////////////////////////////////////////////////////////////////////////////////////////////

class Generation
{
	public:
		Chromosome individuals[GENERACION];
		int size = GENERACION;

		Generation()
		{
		}
		
		
		// Inicializar
		void initialize()
		{
			// Asignar aleatoriamente todos los individuos
			for(int i = 0; i < this->size; i++)
			{
				this->individuals[i]->random();
			}
		}

		
		// Inicializar Generacion_BEM_Elipse
		void reset()
		{
			int i, pos;
			float Max = -FLT_MAX;

			// Hallar el mejor cromosoma
			for(i = 0; i < this->size; i++)
			{
				if(this->individuals[i]->value > Max)
				{
					Max = this->individuals[i]->value;
					pos = i;
				}
			}

			// Copiarlo a la nueva generacion y exterminar el resto
			this->individuals[0] = this->individuals[i];

			// Asignar aleatoriamente los demás individuos
			for(i = 1; i < this->size; i++)
			{
				this->individuals[i]->random();
			}
		}

		
		// Evaluar Generacion_BEM_Elipse
		void evaluate()
		{
			// Para todos los individuos
			for(int i = 0; i < this->size; i++)
			{
				this->individuals[i]->eval();
			}
		}
		
		
		// Asignar probabilidades de selección
		void selectionProbabilities()
		{
			int i;
			float Sumatoria=(float)0.0, Min = 0;

			// Calcular minimo
			for(i = 0; i < this->size; i++)
			{
				if(this->individuals[i]->value < Min)
				{
					Min = this->individuals[i]->value;
				}
			}

			// Calcular sumatoria de costos
			for(i = 0; i < this->size; i++)
			{
				Sumatoria += this->individuals[i]->value - Min;
			}

			// Caso especial
			if(Sumatoria == 0)
			{
				this->individuals[0]->selectionProbability = (float)1 / (float)this->size;

				for(i = 1; i < this->size; i++)
				{
					this->individuals[i]->selectionProbability = this->individuals[0]->selectionProbability + this->individuals[i - 1]->selectionProbability;
				}
			}
			else
			{
				float invSumatoria = (float)1 / Sumatoria;
				
				// Asignar valores de función de densidad
				this->individuals[0]->selectionProbability = (this->individuals[0]->value - Min) * invSumatoria;

				for(i = 1; i < this->size; i++)
				{
					this->individuals[i]->selectionProbability = (this->individuals[i]->value - Min) * invSumatoria + this->individuals[i - 1]->selectionProbability;
				}
			}
		}
		


		// Selección de Baker
		void selectBaker(int M)
		{
			float valor;
			int i, pos;

			// Marcar todos los cromosomas como no seleccionados
			for(i = 0; i < this->size; i++)
			{
				this->individuals[i]->selected = false;
			}

			// Girar la ruleta
			valor = (float)Randfloat(0, 1);

			pos = 0;
			while(this->individuals[pos] < valor)
			{
				pos++;
			}
			
			// Marcar como seleccionado
			this->individuals[pos]->selected = true;

			// Repetir M-1 veces
			for(i = 1; i < M; i++)
			{
				// Salto de tamaño 1/M
				valor += (float)1 / (float)M;
				
				if(valor > (float)1.0)
				{
					valor -= (float)1.0;
				}
				
				pos = 0;
				while(ths->individuals[pos] < valor)
				{
					pos++;
				}
				
				// Marcar como seleccionado
				this->individuals[pos]->selected = true;
			}
		}
}

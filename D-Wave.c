///////////////////////////////////////////////////////////////////////////////////////////////////
// D-Wave Problem
///////////////////////////////////////////////////////////////////////////////////////////////////

// Inclusión de bibliotecas de funciones
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <malloc.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
// Generador de números pseudoaleatorios
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AG_RANDOM
#define _AG_RANDOM

#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

// Rand genera un numero real pseudoaleatorio entre 0 y 1, excluyendo el 1
#define Rand()  (( Seed =(Seed * PRIME) & MASK) * SCALE )

// Randint genera un numero entero entre low y high, ambos incluidos
#define Randint(low,high) (int)(low + (high - (low) + 1) * Rand()) 

// Randfloat genera un numero real entre low y high, incluido low y no incluido high
#define Randfloat(low,high) ((low + (high - (low)) * Rand() ))

#endif
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones auxiliares
///////////////////////////////////////////////////////////////////////////////////////////////////

// Función que devuelve el máximo entre dos elementos
float Max(float uno, float dos)
{
	return (uno > dos)? uno: dos;
}


// Función que devuelve el mínimo entre dos elementos
float Min(float uno, float dos)
{
	return (uno < dos)? uno: dos;
}


// Sign
float Sign(float a)
{
	return (a < 0)? (float)(-1): (float)(1);
}


// Error
void Error(char *s)
{
	fprintf(stderr, "Error: %s.", s);
	exit(1);
}
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
// Algoritmos Evolutivos
///////////////////////////////////////////////////////////////////////////////////////////////////


// Definición de parámetros
#define ITERACIONES 15000	//
#define GENERACIONES 300 //
#define POBLACION 50 //
#define MUTACION 0.4
#define CRUCE 0.2
#define RD 0.5
#define FD 0.01
#define NBITS 10 //
#define K_TORNEO 4



///////////////////////////////////////////////////////////////////////////////////////////////////
// TdA
///////////////////////////////////////////////////////////////////////////////////////////////////


// Creación de un BEM_Placa
BEM_Placa Crear_BEM_Placa()
{
	BEM_Placa Nueva;

	Nueva=(BEM_Placa)malloc(sizeof(TDA_BEM_Placa));
	if(Nueva==NULL)
		Error("sin memoria para BEM_Placa");

	return Nueva;
}



// Destrucción de un BEM_Placa
void Destruir_BEM_Placa(BEM_Placa X)
{
	free(X);
}



// Función para leer el problema desde fichero
BEM_Placa Leer_BEM_Placa(char * s)
{
	FILE *f;
	BEM_Placa Nueva;

	// Abrir fichero de lectura
	f=fopen(s, "rb");
	if(f==NULL)
		Error("no se puede leer BEM_Placa");

	// Crear nueva placa
	Nueva=Crear_BEM_Placa();

	// Leer valores y asignarlos
	fscanf(f, "%f %f %f %f %f %f", &Nueva->U1, &Nueva->U2, 
		&Nueva->V1,	&Nueva->V2, &Nueva->Modulo_Poisson,
		&Nueva->Modulo_Rigidez);

	return Nueva;
}




// Creación de un BEM_Elipses
BEM_Elipses Crear_BEM_Elipses(BEM_Placa X)
{
	int i, j;
	BEM_Elipses Nuevo;

	// Reservar memoria
	Nuevo=(BEM_Elipses)malloc(sizeof(TDA_BEM_Elipses));
	if(Nuevo==NULL)
		Error("sin memoria para BEM_Elipses");

	// Inicializar costo
	Nuevo->CostoActual=0.0;

	// Tamaño por defecto = N_ELIPSES
	Nuevo->Tam=N_ELIPSES;
	Nuevo->Problema=X;

	// Límites
	Nuevo->NLims=14;
	Nuevo->Lim[0]=(float)(X->U1+(X->U2-X->U1)*0.025);
	Nuevo->Lim[1]=(float)(Nuevo->Lim[0]+(X->U2-X->U1)*0.95);
	Nuevo->Lim[2]=(float)(X->V1+(X->V2-X->V1)*0.025);
	Nuevo->Lim[3]=(float)(Nuevo->Lim[2]+(X->V2-X->V1)*0.95);
	Nuevo->Lim[4]=(float)(0.025*Min(X->U2-X->U1, X->V2-X->V1));
	Nuevo->Lim[5]=(float)(0.55*sqrt(pow(X->U2-X->U1, 2)+pow(X->V2-X->V1, 2)));
	Nuevo->Lim[6]=(float)(0.025*Min(X->U2-X->U1, X->V2-X->V1));
	Nuevo->Lim[7]=(float)(Max(X->U2-X->U1, X->V2-X->V1));
	Nuevo->Lim[8]=(float)(0.001*Min(X->U2-X->U1, X->V2-X->V1));
	Nuevo->Lim[9]=(float)(0.5*Max(X->U2-X->U1, X->V2-X->V1));
	Nuevo->Lim[10]=(float)0;
	Nuevo->Lim[11]=(float)PI;
	Nuevo->Lim[12]=(float)-0.999;
	Nuevo->Lim[13]=(float)0.999;

	// Asignar valores por defecto
	for(i=0; i<MAXELIPSES; i++)
	{
		Nuevo->Elipse[i].NParams=7;

		for(j=0; j<Nuevo->Elipse[i].NParams; j++)
			Nuevo->Elipse[i].V[j]=0;
			
		Nuevo->Elipse[i].X=&(Nuevo->Elipse[i].V[0]);
		Nuevo->Elipse[i].Y=&(Nuevo->Elipse[i].V[1]);
		Nuevo->Elipse[i].R=&(Nuevo->Elipse[i].V[2]);
		Nuevo->Elipse[i].H=&(Nuevo->Elipse[i].V[3]);
		Nuevo->Elipse[i].Beta=&(Nuevo->Elipse[i].V[4]);
		Nuevo->Elipse[i].Theta=&(Nuevo->Elipse[i].V[5]);
		Nuevo->Elipse[i].Factor=&(Nuevo->Elipse[i].V[6]);
	}

	return Nuevo;
}



// Destrucción de un BEM_Elipses
void Destruir_BEM_Elipses(BEM_Elipses X)
{
	free(X);
}




//
// Operadores 
//

// Asignación aleatoria de solución BEM_Elipses
void AsignarAleatorio_BEM_Elipses(BEM_Elipses X, int * Seed1)
{
	int i, j, Seed=*Seed1;

	// Para todas las elipses del cromosoma
	for(i=0; i<X->Tam; i++)
	{
		// Generar los parámetros
		for(j=0; j<X->Elipse[i].NParams; j++)
			X->Elipse[i].V[j]=(float)Randfloat(X->Lim[j*2], X->Lim[2*j+1]);
	}

	*Seed1=Seed;
}



// Copia de estructuras BEM_Elipses
void Copia_BEM_Elipses(BEM_Elipses X, BEM_Elipses Y)
{
	int i, j;

	// Copiar tamaño, costo y problema
	Y->Tam=X->Tam;
	Y->CostoActual=X->CostoActual;
	Y->Problema=X->Problema;

	// Para todas las elipses
	for(i=0; i<MAXELIPSES; i++)
	{
		// Copiar todo el contenido
		for(j=0; j<Y->Elipse[i].NParams; j++)
			Y->Elipse[i].V[j]=X->Elipse[i].V[j];
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////





// Función de Fitness. Parametrización Elíptica Pura
// Cálculo de la función objetivo BEM_Elipses
float Objetivo_BEM_Elipse(BEM_Elipses X)
{
	int ic,in;
	float funcval, funcval1;

	// Reservar memoria
	TElipse **Elips=Crear_VTElipse(MaxNCavs+1);

	// Comprobar que se trata de parametrización elíptica
	Param_Elipses(Elips, X);
				
	funcval=0.0;

	// Para todas las condiciones de borde (experimentos)
	for(b=1; b<=NCondb; b++) // ? Sustitución de ii por b
	{
		BEM2(Elips);
		funcval1=0.0;

		// Para todos los contornos externos
		for(ic=1; ic<=NContex; ic++)
		{
			// Sumatoria interna de la raiz en Fitness
			// Para todos los nodos
			for(in=Contorno[ic]->NodoIni; in<=Contorno[ic]->NodoFin; in++)
				funcval1+=(float)(Contorno[ic]->XAct[b]*pow((Nodo[in]->Ux - ReNodo[in]->Ux[b]), 2)
				+pow(((Nodo[in]->Tx - ReNodo[in]->Tx[b])/ModRigidez),2)
				+Contorno[ic]->YAct[b]*pow((Nodo[in]->Uy - ReNodo[in]->Uy[b]),2)
				+pow(((Nodo[in]->Ty - ReNodo[in]->Ty[b])/ModRigidez),2));
		}
		
		funcval+=funcval1;
	}

	// Offciclos On
	// Función Fitness, se extrae la raiz y se realiza el resto del cálculo
	funcval=(float)(10.0-1000.0*pow((funcval/NCondb), 0.5));

	// Liberar memoria
	Destruir_VTElipse(Elips, MaxNCavs+1);

	return funcval;
}



// Creación de una Generacion_BEM_Elipse de tamaño N
Generacion_BEM_Elipse Crear_Generacion_BEM_Elipse(int N)
{
	Generacion_BEM_Elipse Nuevo;

	// Reservar memoria para la estructura de datos
	Nuevo=(Generacion_BEM_Elipse)malloc(sizeof(TDA_Generacion_BEM_Elipse));
	if(Nuevo==NULL)
		Error("sin memoria para Generacion_BEM_Elipse");

	Nuevo->Poblacion=(BEM_Elipses*)malloc(sizeof(BEM_Elipses)*N);
	if(Nuevo->Poblacion==NULL)
		Error("sin memoria para Poblacion");

	Nuevo->Ps=(float*)malloc(sizeof(float)*N);
	if(Nuevo->Ps==NULL)
		Error("sin memoria para Ps");

	Nuevo->Selec=(int*)malloc(sizeof(int)*N);
	if(Nuevo->Ps==NULL)
		Error("sin memoria para Selec");

	// Asignación de valores conocidos
	Nuevo->Tam=N;
	
	return Nuevo;
}



// Destrucción de un Generacion_BEM_Elipse
void Destruir_Generacion_BEM_Elipse(Generacion_BEM_Elipse X)
{
	// Liberar memoria
	free(X->Poblacion);
	free(X->Ps);
	free(X);
}



// Inicializar Generacion_BEM_Elipse
void InicGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, BEM_Placa 
							   Problema, int * Seed1)
{
	BEM_Elipses Nuevo;
	int i, Seed=*Seed1;

	// Asignar aleatoriamente todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(Problema);
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);
		G->Poblacion[i]=Nuevo;
	}

	*Seed1=Seed;
}



// Inicializar Generacion_BEM_Elipse
void ReiniciarGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, int * Seed1)
{
	BEM_Elipses Nuevo, Best;
	int i, pos, Seed=*Seed1;
	float Maximo=-FLT_MAX;

	// Hallar el mejor cromosoma
	for(i=0; i<G->Tam; i++)
	{
		if(G->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo a la nueva generacion y exterminar el resto
	Best=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
	Copia_BEM_Elipses(G->Poblacion[pos], Best);
	Exterminar_Generacion_BEM_Elipse(G);
	G->Poblacion[0]=Best;

	// Asignar aleatoriamente los demás individuos
	for(i=1; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);
		G->Poblacion[i]=Nuevo;
	}

	*Seed1=Seed;
}



// Exterminar Generacion_BEM_Elipse
void Exterminar_Generacion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;

	for(i=0; i<G->Tam; i++)
		Destruir_BEM_Elipses(G->Poblacion[i]);
}




// Asignar probabilidades de selección
void AsignarPs_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;
	float Sumatoria=(float)0.0, minimo=0;

	// Calcular minimo
	for(i=0; i<G->Tam; i++)
	{
		if(G->Poblacion[i]->CostoActual<minimo)
			minimo=G->Poblacion[i]->CostoActual;
	}

	// Calcular sumatoria de costos
	for(i=0; i<G->Tam; i++)
		Sumatoria+=G->Poblacion[i]->CostoActual-minimo;

	// Caso especial
	if(Sumatoria==0)
	{
		G->Ps[0]=(float)1/(float)G->Tam;

		for(i=1; i<G->Tam; i++)
			G->Ps[i]=1/(float)G->Tam+G->Ps[i-1];
	}
	else
	{
		// Asignar valores de función de densidad
		G->Ps[0]=(G->Poblacion[0]->CostoActual-minimo)/Sumatoria;

		for(i=1; i<G->Tam; i++)
			G->Ps[i]=(G->Poblacion[i]->CostoActual-minimo)/Sumatoria+G->Ps[i-1];
	}
}





// Selección de Baker
void SeleccionBaker_BEM_Elipse(Generacion_BEM_Elipse G, 
							   int *Seed1, int M)
{
	float valor;
	int Seed=*Seed1;
	int i, pos;

	// Marcar todos los cromosomas como no seleccionados
	for(i=0; i<G->Tam; i++)
		G->Selec[i]=0;

	// Girar la ruleta
	valor=(float)Randfloat(0, 1);

	pos=0;
	while(G->Ps[pos]<valor)
		pos++;
	
	// Marcar como seleccionado
	G->Selec[pos]=1;

	// Repetir M-1 veces
	for(i=1; i<M; i++)
	{
		// Salto de tamaño 1/M
		valor+=(float)1/(float)M;
		
		if(valor>(float)1.0)
			valor-=(float)1.0;
		
		pos=0;
		while(G->Ps[pos]<valor)
			pos++;
		
		// Marcar como seleccionado
		G->Selec[pos]++;
	}

	*Seed1=Seed;
}




// Selección por torneo
void SeleccionTorneo_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1)
{
	float Mejor;
	int Seed=*Seed1;
	int i, j, pos, posMejor, Marca[POBLACION];


	// Marcar todos los cromosomas como no seleccionados
	for(i=0; i<G->Tam; i++)
		G->Selec[i]=0;

	// Seleccionar Tam cromosomas
	for(i=0; i<G->Tam; i++)
	{
		// Desmarcar todos los cromosomas
		for(j=0; j<G->Tam; j++)
			Marca[j]=0;

		Mejor=-FLT_MAX;

		// Para K_TORNEO cromosomas
		for(j=0; j<K_TORNEO; j++)
		{
			// Seleccionar sin reemplazamiento un cromosoma
			do{
				pos=Randint(0, G->Tam-1);
			}while(Marca[pos]);

			// Marcarlo
			Marca[pos]++;

			// Quedarse con el mejor
			if(G->Poblacion[pos]->CostoActual > Mejor)
			{
				Mejor=G->Poblacion[pos]->CostoActual;
				posMejor=pos;
			}
		}

		// Seleccionar el mejor
		G->Selec[posMejor]++;
	}

	*Seed1=Seed;
}




// Evaluar Generacion_BEM_Elipse
void EvGeneracion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;

	// Para todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		// Evaluar la función objetivo
		G->Poblacion[i]->CostoActual=Objetivo_BEM_Elipse(G->Poblacion[i]);
	}
}




// Evaluar Generacion_BEM_Elipse con Fitness Sharing
void Ev2Generacion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i, j, *Dist, longitud;
	long Sumatoria;

	// Longitud del cromosoma
	longitud=NBITS*G->Poblacion[0]->Tam*G->Poblacion[0]->Elipse[0].NParams;

	// Reservar memoria
	Dist=(int*)malloc(sizeof(int)*G->Tam*G->Tam);
	if(Dist==NULL)
		Error("sin memoria para Dist");

	// Calcular distancias entre individuos
	for(i=0; i<G->Tam; i++)
	{
		for(j=i; j<G->Tam; j++)
		{
			if(i!=j)
				Dist[i*G->Tam+j]=Distancia_BEM_Elipses(G->Poblacion[i], G->Poblacion[j]);
			else
				Dist[i*G->Tam+j]=0;
			Dist[j*G->Tam+i]=Dist[i*G->Tam+j];
		}
	}

	// Para todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		Sumatoria=0;

		// Para el resto de individuos
		for(j=0; j<G->Tam; j++)
		{
			if(Dist[i*G->Tam+j]<longitud)
				Sumatoria+=(1-Dist[i*G->Tam+j])/longitud;
		}

		// Evaluar la función objetivo
		G->Poblacion[i]->CostoActual*=Sumatoria;
	}

	// Liberar memoria
	free(Dist);
}






// Algoritmo Genético Generacional para el problema BEM_Elipse
BEM_Elipses AGG_BEM_Elipse(BEM_Placa Problema, char * f_graficos,
						   int Seed)
{
	Generacion_BEM_Elipse G1, G2;
	BEM_Elipses Mejor;
	int i, j, iteraciones, pos;
	float Maximo=-FLT_MAX;


	iteraciones=(int)(GENERACIONES);

	// Crear BEM_Elipses solución
	Mejor=Crear_BEM_Elipses(Problema);


	//
	// AGG
	//

	// Crear ecosistema
	G1=Crear_Generacion_BEM_Elipse(POBLACION);
	G2=Crear_Generacion_BEM_Elipse(POBLACION);

	// Crear primera generación
	InicGeneracion_BEM_Elipse(G1, Problema, &Seed);

	// Evaluar generación
	EvGeneracion_BEM_Elipse(G1);


	// ITERACIONES
	for(i=0; i<iteraciones; i++)
	{
		// Verbose
		if(!(i%8))
		{		
			// Buscar el mejor cromosoma
			Maximo=-FLT_MAX;
			for(j=0; j<G1->Tam; j++)
			{
				if(G1->Poblacion[j]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[j]->CostoActual;
					pos=j;
				}
			}

			printf("%d %f\n", i, G1->Poblacion[pos]->CostoActual);
			Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);
		}

		// Selección de cromosomas
		AsignarPs_BEM_Elipse(G1);
		SeleccionBaker_BEM_Elipse(G1, &Seed, POBLACION);

		// Siguiente generación
		for(pos=0, j=0; j<G1->Tam; j++)
		{
			while(G1->Selec[j])
			{
				G2->Poblacion[pos]=Crear_BEM_Elipses(Problema);
				Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
				pos++;
				G1->Selec[j]--;
			}
		}
		
		// Cruce
		AGG_Cruce_BEM_Elipse(G2, &Seed);

		// Mutación
		for(j=0; j<G2->Tam; j++)
		{
			if(Randfloat(0, 1)<MUTACION)
			{
				AG_Mutacion_BEM_Elipse(G2->Poblacion[j], &Seed);
				AG_Mutacion2_BEM_Elipse(G2->Poblacion[j], &Seed);
			}
		}

		// Evaluar
		EvGeneracion_BEM_Elipse(G2);

		// Envejecimiento de la población con elitismo
		SustGeneracion_BEM_Elipse(G1, G2);
		
		/*// Exterminar generación anterior
		Exterminar_Generacion_BEM_Elipse(G1);
		// Envejecimiento de la población
		for(j=0; j<G2->Tam; j++) G1->Poblacion[j]=G2->Poblacion[j];*/
	}


	// Buscar el mejor cromosoma
	Maximo=-FLT_MAX;
	for(i=0; i<G1->Tam; i++)
	{
		if(G1->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G1->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo y exterminar al resto
	Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
	Exterminar_Generacion_BEM_Elipse(G1);

	// Liberar memoria
	Destruir_Generacion_BEM_Elipse(G1);
	Destruir_Generacion_BEM_Elipse(G2);

	return Mejor;
}




//
// Operadores Genéticos
//


// Operador Cruce
void AG_Cruce_BEM_Elipse(BEM_Elipses Padre, BEM_Elipses Madre, 
						 BEM_Elipses Hijo1, BEM_Elipses Hijo2, 
						 int * Seed1)
{
	int Seed=*Seed1, valor, i, j;

	// Copiar Padres en los Hijos
	Copia_BEM_Elipses(Padre, Hijo1);
	Copia_BEM_Elipses(Madre, Hijo2);


	// Cruce simple

	// Elegir punto de cruce
	valor=Randint(0, Padre->Tam-1);

	// Cruzar
	// Para todas las elipses
	for(i=valor; i<Padre->Tam; i++)
	{
		// Para todos los genes
		for(j=0; j<Padre->Elipse[0].NParams; j++)
		{
			Hijo1->Elipse[i].V[j]=Madre->Elipse[i].V[j];
			Hijo2->Elipse[i].V[j]=Padre->Elipse[i].V[j];
		}
	}

	*Seed1=Seed;
}




// Operador Mutación
void AG_Mutacion_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1)
{
	int Seed=*Seed1, i, j;

	// Para todas las elipses
	for(i=0; i<Antiguo->Tam; i++)
	{
		// Para todos los parámetros
		for(j=0; j<Antiguo->Elipse[0].NParams; j++)
		{
			// Ratio de desplazamiento
			if(Randfloat(0,1) < RD)
			{
				// Incremento
				if(Randfloat(0,1) < 0.5)
					Antiguo->Elipse[i].V[j]+=(float)FD*(Antiguo->Lim[2*j+1] 
						- Antiguo->Lim[2*j]);
				// Disminución
				else
					Antiguo->Elipse[i].V[j]-=(float)FD*(Antiguo->Lim[2*j+1]
						- Antiguo->Lim[2*j]);
			
				// Ajuste
				if(Antiguo->Elipse[i].V[j] < Antiguo->Lim[2*j])
					Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j];
				if(Antiguo->Elipse[i].V[j] > Antiguo->Lim[2*j+1])
					Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j+1];
			}
		}
	}

	*Seed1=Seed;
}




// Operador Mutación Aleatorio Simple
void AG_Mutacion2_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1)
{
	int Seed=*Seed1, Elip, Gen;
	float MinV, MaxV;

	// Seleccionar Elipse
	Elip=Randint(0, Antiguo->Tam-1);

	// Seleccionar gen
	Gen=Randint(0, Antiguo->Elipse[0].NParams-1);

	if(Gen!=(Antiguo->Elipse[0].NParams-1))
	{
		MinV=Antiguo->Lim[2*Gen];
		MaxV=Antiguo->Lim[2*Gen+1];
		Antiguo->Elipse[Elip].V[Gen]=(float)(MinV+Randfloat(0,1)*(MaxV-MinV));
	}

	*Seed1=Seed;
}




// Realizar una sustitución de cromosomas con elitismo
void SustGeneracion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2)
{
	int i, j, *Pos, Maxpos;
	float Max;

	// Reservar memoria
	Pos=(int*)malloc(sizeof(int)*G1->Tam*2);
	if(Pos==NULL)
		Error("sin memoria para Pos");

	// Inicializar cromosomas elegidos
	for(i=0; i<G1->Tam*2; i++)
		Pos[i]=0;

	// Elegir los N mejores
	for(i=0; i<G1->Tam; i++)
	{
		Max=-FLT_MAX;
		Maxpos=-1;

		for(j=0; j<G1->Tam*2; j++)
		{
			// Si no está seleccionado
			if(!Pos[j])
			{
				// Buscar en generación anterior
				if(j<G1->Tam)
				{
					if(G1->Poblacion[j]->CostoActual>Max)
					{
						Max=G1->Poblacion[j]->CostoActual;
						Maxpos=j;
					}
				}
				// Buscar en generación nueva
				else
				{
					if(G2->Poblacion[j-G1->Tam]->CostoActual>Max)
					{
						Max=G2->Poblacion[j-G1->Tam]->CostoActual;
						Maxpos=j;
					}
				}
			}
		}

		// Seleccionar
		Pos[Maxpos]++;
	}

	// Eliminar los no elegidos
	for(i=0; i<G1->Tam*2; i++)
	{
		if(!Pos[i])
		{
			if(i<G1->Tam)
				Destruir_BEM_Elipses(G1->Poblacion[i]);
			else
				Destruir_BEM_Elipses(G2->Poblacion[i-G1->Tam]);
		}
	}

	// Salvar los seleccionados
	j=0;
	for(i=0; i<G1->Tam*2; i++)
	{
		if(Pos[i])
		{
			if(i<G1->Tam)
				G1->Poblacion[j]=G1->Poblacion[i];
			else
				G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
			j++;
		}
	}

	// Liberar memoria
	free(Pos);
}




// Cruce generacional en Generacion_BEM_Elipse
void AGG_Cruce_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1)
{
	BEM_Elipses aux, Hijo1, Hijo2;
	int Seed=*Seed1;
	int i, pos, iteraciones;

	iteraciones=(int)(G->Tam*CRUCE);

	// Barajar
	for(i=0; i<G->Tam; i++)
	{
		pos=Randint(0, G->Tam-1);
		aux=G->Poblacion[i];
		G->Poblacion[i]=G->Poblacion[pos];
		G->Poblacion[pos]=aux;
	}
		
	// Cruzar
	for(i=0; i<iteraciones; i+=2)
	{
		Hijo1=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		Hijo2=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		
		AG_Cruce_BEM_Elipse(G->Poblacion[i], G->Poblacion[i+1], 
		Hijo1, Hijo2, &Seed);
		
		Destruir_BEM_Elipses(G->Poblacion[i]);
		Destruir_BEM_Elipses(G->Poblacion[i+1]);
		G->Poblacion[i]=Hijo1;
		G->Poblacion[i+1]=Hijo2;
	}

	*Seed1=Seed;
}




// Distancia entre BEM_Elipses
int Distancia_BEM_Elipses(BEM_Elipses C1, BEM_Elipses C2)
{
	char *cx, *cy;
	short c1, c2, uno=1;
	int Distancia=0, i, j, k;

	// Para todas las reglas
	for(i=0; i<C1->Tam; i++)
	{
		// Para todos los genes reales
		for(j=0; j<C1->Elipse[0].NParams; j++)
		{
			cx=(char*)&c1;
			cy=(char*)&C1->Elipse[i].V[j];
			*cx=*cy;
			*(cx+1)=*(cy+1);

			cx=(char*)&c2;
			cy=(char*)&C2->Elipse[i].V[j];
			*cx=*cy;
			*(cx+1)=*(cy+1);

			c1=c1 ^ c2;
			
			// Para los NBits primeros bits
			for(k=0; k<NBITS; k++)
			{
				c2=uno<<k;

				if(c1 & c2)
					Distancia++;
			}
		}
	}

	return Distancia;
}





// Información de la sintaxis de llamada del programa
void Sintaxis();


// Programa principal
main(int argc, char *argv[])
{
	BEM_Placa Placa=NULL;
	BEM_Elipses resultado;
	time_t Inicio, Fin;
	char aux[100];

	int Seed;
	
	// Comprobar el número de argumentos
	if(argc!=1)
	{
		Sintaxis();
	}

	// Semilla
	Seed = atoi(argv[1]);
	
	// Ejecutar simulacion de la placa
	BEM(argv[1]);

	// Leer problema
	strcpy(aux, argv[1]);
	strcat(aux, ".pto");
	Placa=Leer_BEM_Placa(aux);

	// Inicio
	time(&Inicio);

	// Ejecutar el algoritmo seleccionado
	resultado=AGG_BEM_Elipse(Placa, aux, Seed);

	// Fin
	time(&Fin);

	// Presentar resultados
	fprintf(stdout, "Solucion:\n");

	// Solución
	Imprimir_BEM_Elipses(resultado, aux);
	Imprimir_BEM_Elipses(resultado, NULL);

	// Costo final
	fprintf(stdout, "\nCosto=%.4f\n", resultado->CostoActual);
	
	// Tiempo de ejecución
	fprintf(stdout, "Tiempo=%.6fseg\n",difftime(Fin,Inicio));

	// Liberar memoria
	Destruir_BEM_Placa(Placa);

	return 0;
}



// Información de la sintaxis de llamada del programa y salir
void Sintaxis()
{
	printf("Sintaxis:\n");
	printf("D-Wave SEED\n");
	printf("SEED es la semilla del generador pseudoaleatorio.\n");

	exit(1);
}

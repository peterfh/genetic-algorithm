
			/********************************/
			/********************************/



// Inclusi�n de bibliotecas
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

#include "../BEM/Funcs_Contorno.h"
#include "../BEM/Funcs_Aux.h"
#include "Genetic_BEM.h"
#include "BEM_Elipse.h"
#include "AG.h"


// Informaci�n de la sintaxis de llamada del programa
void Sintaxis();


// Programa principal
main(int argc, char *argv[])
{
	BEM_Placa Placa=NULL;
	BEM_Elipses resultado;
	time_t Inicio, Fin;
	char aux[100];
	int Seed;
	
	// Comprobar el n�mero de argumentos
	if(argc!=4)
		Sintaxis();

	// Semilla
	Seed=atoi(argv[3]);
	
	// Ejecutar simulacion de la placa
	system("limpiar.bat");
	BEM(argv[1]);

	// Leer problema
	strcpy(aux, argv[1]);
	strcat(aux, ".pto");
	Placa=Leer_BEM_Placa(aux);

	// Inicio
	time(&Inicio);

	// Ejecutar el algoritmo seleccionado
	switch(argv[2][0])
	{
	case 'G':
		resultado=AGG_BEM_Elipse(Placa, aux, Seed);
		break;

	case 'L':
		resultado=AGGLV_BEM_Elipse(Placa, aux, Seed);
		break;

	case 'C':
		resultado=AGCHC_BEM_Elipse(Placa, aux, Seed);
		break;

	case 'E':
		if (argv[2][1] == '1'){
			resultado=Ee1_BEM_Elipse(Placa, aux, Seed);
			break;
		}
		
		if (argv[2][1] == '2'){
			resultado=Ee2_BEM_Elipse(Placa, aux, Seed);
			break;
		}

		Sintaxis();

	default:
		Sintaxis();
	}

	// Fin
	time(&Fin);

	// Presentar resultados
	fprintf(stdout, "Solucion:\n");

	// Soluci�n
	Imprimir_BEM_Elipses(resultado, aux);
	Imprimir_BEM_Elipses(resultado, NULL);

	// Costo final
	fprintf(stdout, "\nCosto=%.4f\n", resultado->CostoActual);
	
	// Tiempo de ejecuci�n
	fprintf(stdout, "Tiempo=%.6fseg\n",difftime(Fin,Inicio));

	// Liberar memoria
	Destruir_BEM_Placa(Placa);

	return 0;
}



// Informaci�n de la sintaxis de llamada del programa y salir
void Sintaxis()
{
	printf("Sintaxis:\n");
	printf("GenBEM FICH_ENT ALG SEED\n");
	printf("FICH_ENT es el nombre del fichero de entrada.\n");
	printf("\tA FICH_ENT se a�ade la extension DAT.\n");
	printf("ALG indica el Algoritmo a utilizar:\n");
	printf("\tG para Algoritmo AG Generacional.\n");
	printf("\tL para Algoritmo AGG Longitud Variable.\n");
	printf("\tE1 para Estrategias de Evoluci�n 1,1.\n");
	printf("\tE2 para Estrategias de Evoluci�n 15,105.\n");
	printf("\tC para Algoritmo CHC.\n");
	printf("SEED es la semilla del generador pseudoaleatorio.\n");

	exit(1);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Genetic Algorithms
///////////////////////////////////////////////////////////////////////////////////////////////////

// Inclusión de bibliotecas de funciones
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <malloc.h>


#include "class_Chromosome.hxx"
#include "class_Generation.hxx"


///////////////////////////////////////////////////////////////////////////////////////////////////
// Generador de números pseudoaleatorios
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AG_RANDOM
#define _AG_RANDOM

#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

// Rand genera un numero real pseudoaleatorio entre 0 y 1, excluyendo el 1
#define Rand()  (( Seed =(Seed * PRIME) & MASK) * SCALE )

// Randint genera un numero entero entre low y high, ambos incluidos
#define Randint(low,high) (int)(low + (high - (low) + 1) * Rand()) 

// Randfloat genera un numero real entre low y high, incluido low y no incluido high
#define Randfloat(low,high) ((low + (high - (low)) * Rand() ))

#endif
///////////////////////////////////////////////////////////////////////////////////////////////////





///////////////////////////////////////////////////////////////////////////////////////////////////
// Algoritmos Evolutivos
///////////////////////////////////////////////////////////////////////////////////////////////////


// Definición de parámetros
#define ITERACIONES 15000	//
#define GENERACIONES 300 //
#define POBLACION 50 //
#define MUTACION 0.4
#define CRUCE 0.2
#define RD 0.5
#define FD 0.01
#define NBITS 10 //
#define K_TORNEO 4





///////////////////////////////////////////////////////////////////////////////////////////////////
// Class Genetic Algorithm
///////////////////////////////////////////////////////////////////////////////////////////////////


class GA
{
	private:
		Generation generation;
								   
	public:
		int generaciones = 50;
		
		// Algoritmo Genético Generacional
		void GGA()
		{
			Generacion_BEM_Elipse G1, G2;
			BEM_Elipses Mejor;
			int i, j, iteraciones, pos;
			float Maximo=-FLT_MAX;

			//
			// AGG
			//

			// Mejor solución actual
			Mejor = new Chromosome;

			// Crear ecosistema
			G1 = new Generation(POBLACION);
			G2 = new Generation(POBLACION);

			// Crear primera generación
			G1->initialize();

			// Evaluar generación
			G1->eval();

			// Generaciones
			for(i = 0; i < this.generaciones; i++)
			{
				// Selección de cromosomas
				G1->selectionProbabilities();
				G1->selectBaker(POBLACION);

				// Siguiente generación
				for(pos = 0, j = 0; j < G1->size; j++)
				{
					while(G1->Selec[j])
					{
						G2->Poblacion[pos] = Crear_BEM_Elipses(Problema);
						Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
						pos++;
						G1->Selec[j]--;
					}
				}
				
				// Cruce
				G2->cross();

				// Mutación
				for(j = 0; j < G2->size; j++)
				{
					if(Randfloat(0, 1) < MUTACION)
					{
						G2->individuals[j]->mutation();
					}
				}

				// Evaluar
				G2->eval();

				// Envejecimiento de la población con elitismo
				reset(G1, G2);
				
				/*// Exterminar generación anterior
				Exterminar_Generacion_BEM_Elipse(G1);
				// Envejecimiento de la población
				for(j=0; j<G2->Tam; j++) G1->Poblacion[j]=G2->Poblacion[j];*/
			}

			// Buscar el mejor cromosoma
			Mejor = G1->best();

			// Liberar memoria
			Destruir_Generacion_BEM_Elipse(G1);
			Destruir_Generacion_BEM_Elipse(G2);

			return Mejor;
		}
		
		
		//
		// Operadores Genéticos
		//
	


		// Realizar una sustitución de cromosomas con elitismo
		void generationChange()
		{
			int i, j, *Pos, Maxpos;
			float Max;

			// Reservar memoria
			Pos=(int*)malloc(sizeof(int)*G1->Tam*2);
			if(Pos==NULL)
				Error("sin memoria para Pos");

			// Inicializar cromosomas elegidos
			for(i=0; i<G1->Tam*2; i++)
				Pos[i]=0;

			// Elegir los N mejores
			for(i=0; i<G1->Tam; i++)
			{
				Max=-FLT_MAX;
				Maxpos=-1;

				for(j=0; j<G1->Tam*2; j++)
				{
					// Si no está seleccionado
					if(!Pos[j])
					{
						// Buscar en generación anterior
						if(j<G1->Tam)
						{
							if(G1->Poblacion[j]->CostoActual>Max)
							{
								Max=G1->Poblacion[j]->CostoActual;
								Maxpos=j;
							}
						}
						// Buscar en generación nueva
						else
						{
							if(G2->Poblacion[j-G1->Tam]->CostoActual>Max)
							{
								Max=G2->Poblacion[j-G1->Tam]->CostoActual;
								Maxpos=j;
							}
						}
					}
				}

				// Seleccionar
				Pos[Maxpos]++;
			}

			// Eliminar los no elegidos
			for(i=0; i<G1->Tam*2; i++)
			{
				if(!Pos[i])
				{
					if(i<G1->Tam)
						Destruir_BEM_Elipses(G1->Poblacion[i]);
					else
						Destruir_BEM_Elipses(G2->Poblacion[i-G1->Tam]);
				}
			}

			// Salvar los seleccionados
			for(i = 0, j = 0; i < G1->Tam; i++)
			{
				if(Pos[i])
				{
					if(i<G1->Tam)
					{
						G1->Poblacion[j]=G1->Poblacion[i];
					}
					else
					{
						G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
					}

					j++;
				}
			}

			// Liberar memoria
			free(Pos);
		}
};






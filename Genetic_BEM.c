
		//
		// Fichero de Cabecera Genetic_BEM
		// Declaraci�n de Funciones
		//


// Inclusi�n de bibliotecas de funciones y TDAs
#include<math.h>

#include "../BEM/TDA_Contorno.h"
#include "../BEM/Parametros.h"
#include "../BEM/Funcs_Contorno.h"
#include "../BEM/Funcs_Aux.h"
#include "../BEM/Globales.h"

#include "BEM_Elipse.h"
#include "Genetic_BEM.h"




// Funci�n de Fitness. Parametrizaci�n El�ptica Pura
// C�lculo de la funci�n objetivo BEM_Elipses
float Objetivo_BEM_Elipse(BEM_Elipses X)
{
	int ic,in;
	float funcval, funcval1;

	// Reservar memoria
	TElipse **Elips=Crear_VTElipse(MaxNCavs+1);

	// Comprobar que se trata de parametrizaci�n el�ptica
	Param_Elipses(Elips, X);
				
	funcval=0.0;

	// Para todas las condiciones de borde (experimentos)
	for(b=1; b<=NCondb; b++) // ? Sustituci�n de ii por b
	{
		BEM2(Elips);
		funcval1=0.0;

		// Para todos los contornos externos
		for(ic=1; ic<=NContex; ic++)
		{
			// Sumatoria interna de la raiz en Fitness
			// Para todos los nodos
			for(in=Contorno[ic]->NodoIni; in<=Contorno[ic]->NodoFin; in++)
				funcval1+=(float)(Contorno[ic]->XAct[b]*pow((Nodo[in]->Ux - ReNodo[in]->Ux[b]), 2)
				+pow(((Nodo[in]->Tx - ReNodo[in]->Tx[b])/ModRigidez),2)
				+Contorno[ic]->YAct[b]*pow((Nodo[in]->Uy - ReNodo[in]->Uy[b]),2)
				+pow(((Nodo[in]->Ty - ReNodo[in]->Ty[b])/ModRigidez),2));
		}
		
		funcval+=funcval1;
	}

	// Offciclos On
	// Funci�n Fitness, se extrae la raiz y se realiza el resto del c�lculo
	funcval=(float)(10.0-1000.0*pow((funcval/NCondb), 0.5));

	// Liberar memoria
	Destruir_VTElipse(Elips, MaxNCavs+1);

	return funcval;
}




// Parametrizaci�n pura por elipses
void Param_Elipses(TElipse **elipses, BEM_Elipses X)
{
	int i;

	// Validar las elipses con respecto a los bordes
	ValidarBordes_BEM_Elipses(X);

	// Copiar los par�metros de las elipses en el vector de cavidades
	for(i=1; i<=MaxNCavs; i++)
	{
		elipses[i]->X=*(X->Elipse[i-1].X);
		elipses[i]->Y=*(X->Elipse[i-1].Y);
		elipses[i]->Theta=*(X->Elipse[i-1].Theta);
		elipses[i]->A=*(X->Elipse[i-1].A);
		elipses[i]->B=*(X->Elipse[i-1].B);
	}
}

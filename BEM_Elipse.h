
			/********************************/
			/* Problema BEM para Placa      */
			/* Parametrizaci�n El�ptica     */
			/********************************/



#ifndef _BEM_PARAM_ELIPTICA
#define _BEM_PARAM_ELIPTICA


// Definici�n de constantes
#define PI 3.14159265
#define MAXELIPSES 5		// Deber�a ser siempre = MaxNCavs
#define N_ELIPSES 3			// Para longitud fija de cromosoma
#define EPSILON	0.95		// Constante para evitar tangencias


// Definici�n del TDA BEM_Placa
typedef struct{
	float U1;
	float U2;
	float V1;
	float V2;
	float Modulo_Poisson;
	float Modulo_Rigidez;
} TDA_BEM_Placa;

typedef TDA_BEM_Placa * BEM_Placa;


// Creaci�n de un BEM_Placa
BEM_Placa Crear_BEM_Placa();

// Destrucci�n de un BEM_Placa
void Destruir_BEM_Placa(BEM_Placa X);

// Funci�n para leer el problema desde fichero
BEM_Placa Leer_BEM_Placa(char * s);



// Definici�n del TDA BEM_Elipse 
// Parametrizaci�n Elipse-Pura
typedef struct{
	float *X;		// Abcisa del centro del cono
	float *Y;		// Ordenada del centro del cono
	float *Theta;	// �ngulo del eje mayor con la horizontal
	float *A;		// Semieje primero
	float *B;		// Semieje segundo
	float V[5];		// Contenido real de los par�metros. El resto son punteros.
	int NParams;	// N� de par�metros
} TDA_BEM_Elipse;



// Definici�n del TDA BEM_Elipses
typedef struct{
	int Tam;
	TDA_BEM_Elipse Elipse[MAXELIPSES];
	float CostoActual;
	BEM_Placa Problema;

	// L�mites
	float *MinX;	// Punteros a los l�mites. Par-min, impar-max.
	float *MaxX;
	float *MinY;
	float *MaxY;
	float *MinTheta;
	float *MaxTheta;
	float *MinA;
	float *MaxA;
	float *MinB;
	float *MaxB;
	float Lim[10];		
	int NLims;		// N� de l�mites
} TDA_BEM_Elipses;

typedef TDA_BEM_Elipses * BEM_Elipses;


// Creaci�n de un BEM_Elipses
BEM_Elipses Crear_BEM_Elipses(BEM_Placa X);

// Destrucci�n de un BEM_Elipses
void Destruir_BEM_Elipses(BEM_Elipses X);

// C�lculo de la funci�n objetivo BEM_Elipses
float Objetivo_BEM_Elipses(BEM_Elipses X);

// Asignaci�n aleatoria de soluci�n BEM_Elipses
void AsignarAleatorio_BEM_Elipses(BEM_Elipses X, int * Seed1);

// Copia de estructuras BEM_Elipses
void Copia_BEM_Elipses(BEM_Elipses X, BEM_Elipses Y);

// Imprimir BEM_Elipses
void Imprimir_BEM_Elipses(BEM_Elipses X, char * file);

// Validar BEM_Elipses con respecto a los bordes
void ValidarBordes_BEM_Elipses(BEM_Elipses X);

// Reordenar c�digo gen�tico
void ReordenarCodigo_BEM_Elipses(BEM_Elipses X);

#endif

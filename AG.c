
			/********************************/
			/* Algoritmos Evolutivos		*/
			/********************************/



// Inclusi�n de bibliotecas de funciones y TDAs
#include<stdio.h>
#include<stdlib.h>
#include<float.h>
#include<math.h>
#include<time.h>

#include "../BEM/Funcs_Aux.h"

#include "random.h"
#include "BEM_Elipse.h"
#include "Genetic_BEM.h"
#include "AG.h"



// Definici�n de par�metros
#define ITERACIONES 10000	//
#define GENERACIONES 300 //
#define POBLACION 100 //
#define MUTACION 0.4
#define MUTA_LONGITUD 0.2
#define CRUCE 0.2
#define K_TORNEO 4
#define ELITISMO 3
#define UMBRAL_FITNESS 9.9
#define RD 0.5
#define FD 0.01
#define NBITS 10 //



// Creaci�n de una Generacion_BEM_Elipse de tama�o N
Generacion_BEM_Elipse Crear_Generacion_BEM_Elipse(int N)
{
	Generacion_BEM_Elipse Nuevo;

	// Reservar memoria para la estructura de datos
	Nuevo=(Generacion_BEM_Elipse)malloc(sizeof(TDA_Generacion_BEM_Elipse));
	if(Nuevo==NULL)
		Error("sin memoria para Generacion_BEM_Elipse");

	Nuevo->Poblacion=(BEM_Elipses*)malloc(sizeof(BEM_Elipses)*N);
	if(Nuevo->Poblacion==NULL)
		Error("sin memoria para Poblacion");

	Nuevo->Ps=(float*)malloc(sizeof(float)*N);
	if(Nuevo->Ps==NULL)
		Error("sin memoria para Ps");

	Nuevo->Selec=(int*)malloc(sizeof(int)*N);
	if(Nuevo->Ps==NULL)
		Error("sin memoria para Selec");

	// Asignaci�n de valores conocidos
	Nuevo->Tam=N;
	
	return Nuevo;
}



// Destrucci�n de un Generacion_BEM_Elipse
void Destruir_Generacion_BEM_Elipse(Generacion_BEM_Elipse X)
{
	// Liberar memoria
	free(X->Poblacion);
	free(X->Ps);
	free(X);
}



// Inicializar Generacion_BEM_Elipse
void InicGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, BEM_Placa 
							   Problema, int * Seed1)
{
	BEM_Elipses Nuevo;
	int i, Seed=*Seed1;

	// Asignar aleatoriamente todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(Problema);
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);
		G->Poblacion[i]=Nuevo;
	}

	*Seed1=Seed;
}



// Inicializar Generacion_BEM_Elipse con Longitud Variable
void InicGeneracion_BEM_Elipse_LV(Generacion_BEM_Elipse G, BEM_Placa 
							   Problema, int * Seed1)
{
	BEM_Elipses Nuevo;
	int i, j, k, Seed=*Seed1;
	float total;

	// Crear poblaci�n
	for(i=0; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(Problema);
		G->Poblacion[i]=Nuevo;
	}

	// El primer cromosoma identifica una placa sin errores
	G->Poblacion[0]->Tam=0;

	
	// Asignar n� de elipses por cromosoma
	total=0;
	for(i=1; i<=N_ELIPSES; i++)
		total+=(float)(1.0/i);

	total=G->Tam/total;
	k=1;

	// Para todos los n�s de elipses por cromosoma
	for(i=N_ELIPSES; i>0; i--)
	{
		// Para todas los cromosomas asignados seg�n la distribuci�n
		for(j=0; j<floor(total/i); j++)
		{
			G->Poblacion[k]->Tam=i;
			k++;
		}
	}

	// Asignar el resto de cromosomas con una elipse
	for(i=k; i<G->Tam; i++)
		G->Poblacion[i]->Tam=1;

	
	// Asignar aleatoriamente todos los individuos
	for(i=0; i<G->Tam; i++)
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);

	*Seed1=Seed;
}



// Reiniciar Generacion_BEM_Elipse
void ReiniciarGeneracion_BEM_Elipse(Generacion_BEM_Elipse G, int * Seed1)
{
	BEM_Elipses Nuevo, Best;
	int i, pos, Seed=*Seed1;
	float Maximo=-FLT_MAX;

	// Hallar el mejor cromosoma
	for(i=0; i<G->Tam; i++)
	{
		if(G->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo a la nueva generacion y exterminar el resto
	Best=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
	Copia_BEM_Elipses(G->Poblacion[pos], Best);
	Exterminar_Generacion_BEM_Elipse(G);
	G->Poblacion[0]=Best;

	// Asignar aleatoriamente los dem�s individuos
	for(i=1; i<G->Tam; i++)
	{
		Nuevo=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		AsignarAleatorio_BEM_Elipses(Nuevo, &Seed);
		G->Poblacion[i]=Nuevo;
	}

	*Seed1=Seed;
}



// Exterminar Generacion_BEM_Elipse
void Exterminar_Generacion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;

	for(i=0; i<G->Tam; i++)
		Destruir_BEM_Elipses(G->Poblacion[i]);
}




// Estad�sticas Generacion_BEM_Elipse
void Estadisticas_Generacion_BEM_Elipse(Generacion_BEM_Elipse G, 
									  float * Media, float * Desviacion)
{
	int i;
	float media=0, desviacion=0, aux;

	// Calcular media
	for(i=0; i<G->Tam; i++)
		media+=G->Poblacion[i]->Tam;

	media/=G->Tam;

	// Calcular desviaci�n t�pica
	for(i=0; i<G->Tam; i++)
	{
		aux=G->Poblacion[i]->Tam-media;
		desviacion+=aux*aux;
	}

	desviacion=(float)sqrt(desviacion/G->Tam);

	// Asignar medidas
	*Media=media;
	*Desviacion=desviacion;
}




// Asignar probabilidades de selecci�n
void AsignarPs_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;
	float Sumatoria=(float)0.0, minimo=0;

	// Calcular minimo
	for(i=0; i<G->Tam; i++)
	{
		if(G->Poblacion[i]->CostoActual<minimo)
			minimo=G->Poblacion[i]->CostoActual;
	}

	// Calcular sumatoria de costos
	for(i=0; i<G->Tam; i++)
		Sumatoria+=G->Poblacion[i]->CostoActual-minimo;

	// Caso especial
	if(Sumatoria==0)
	{
		G->Ps[0]=(float)1/(float)G->Tam;

		for(i=1; i<G->Tam; i++)
			G->Ps[i]=1/(float)G->Tam+G->Ps[i-1];
	}
	else
	{
		// Asignar valores de funci�n de densidad
		G->Ps[0]=(G->Poblacion[0]->CostoActual-minimo)/Sumatoria;

		for(i=1; i<G->Tam; i++)
			G->Ps[i]=(G->Poblacion[i]->CostoActual-minimo)/Sumatoria+G->Ps[i-1];
	}
}





// Selecci�n de Baker
void SeleccionBaker_BEM_Elipse(Generacion_BEM_Elipse G, 
							   int *Seed1, int M)
{
	float valor;
	int Seed=*Seed1;
	int i, pos;

	// Marcar todos los cromosomas como no seleccionados
	for(i=0; i<G->Tam; i++)
		G->Selec[i]=0;

	// Girar la ruleta
	valor=(float)Randfloat(0, 1);

	pos=0;
	while(G->Ps[pos]<valor)
		pos++;
	
	// Marcar como seleccionado
	G->Selec[pos]=1;

	// Repetir M-1 veces
	for(i=1; i<M; i++)
	{
		// Salto de tama�o 1/M
		valor+=(float)1/(float)M;
		
		if(valor>(float)1.0)
			valor-=(float)1.0;
		
		pos=0;
		while(G->Ps[pos]<valor)
			pos++;
		
		// Marcar como seleccionado
		G->Selec[pos]++;
	}

	*Seed1=Seed;
}




// Selecci�n por torneo
void SeleccionTorneo_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1)
{
	float Mejor;
	int Seed=*Seed1;
	int i, j, pos, posMejor, Marca[POBLACION];


	// Marcar todos los cromosomas como no seleccionados
	for(i=0; i<G->Tam; i++)
		G->Selec[i]=0;

	// Seleccionar Tam cromosomas
	for(i=0; i<G->Tam; i++)
	{
		// Desmarcar todos los cromosomas
		for(j=0; j<G->Tam; j++)
			Marca[j]=0;

		Mejor=-FLT_MAX;

		// Para K_TORNEO cromosomas
		for(j=0; j<K_TORNEO; j++)
		{
			// Seleccionar sin reemplazamiento un cromosoma
			do{
				pos=Randint(0, G->Tam-1);
			}while(Marca[pos]);

			// Marcarlo
			Marca[pos]++;

			// Quedarse con el mejor
			if(G->Poblacion[pos]->CostoActual > Mejor)
			{
				Mejor=G->Poblacion[pos]->CostoActual;
				posMejor=pos;
			}
		}

		// Seleccionar el mejor
		G->Selec[posMejor]++;
	}

	*Seed1=Seed;
}




// Evaluar Generacion_BEM_Elipse
void EvGeneracion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i;

	// Para todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		// Evaluar la funci�n objetivo
		G->Poblacion[i]->CostoActual=Objetivo_BEM_Elipse(G->Poblacion[i]);
	}
}




// Evaluar Generacion_BEM_Elipse con Fitness Sharing
void Ev2Generacion_BEM_Elipse(Generacion_BEM_Elipse G)
{
	int i, j, *Dist, longitud;
	long Sumatoria;

	// Longitud del cromosoma
	longitud=NBITS*G->Poblacion[0]->Tam*G->Poblacion[0]->Elipse[0].NParams;

	// Reservar memoria
	Dist=(int*)malloc(sizeof(int)*G->Tam*G->Tam);
	if(Dist==NULL)
		Error("sin memoria para Dist");

	// Calcular distancias entre individuos
	for(i=0; i<G->Tam; i++)
	{
		for(j=i; j<G->Tam; j++)
		{
			if(i!=j)
				Dist[i*G->Tam+j]=Distancia_BEM_Elipses(G->Poblacion[i], G->Poblacion[j]);
			else
				Dist[i*G->Tam+j]=0;
			Dist[j*G->Tam+i]=Dist[i*G->Tam+j];
		}
	}

	// Para todos los individuos
	for(i=0; i<G->Tam; i++)
	{
		Sumatoria=0;

		// Para el resto de individuos
		for(j=0; j<G->Tam; j++)
		{
			if(Dist[i*G->Tam+j]<longitud)
				Sumatoria+=(1-Dist[i*G->Tam+j])/longitud;
		}

		// Evaluar la funci�n objetivo
		G->Poblacion[i]->CostoActual*=Sumatoria;
	}

	// Liberar memoria
	free(Dist);
}






// Algoritmo Gen�tico Generacional para el problema BEM_Elipse
BEM_Elipses AGG_BEM_Elipse(BEM_Placa Problema, char * f_graficos, 
						   int Seed)
{
	Generacion_BEM_Elipse G1, G2;
	BEM_Elipses Mejor;
	int i, j, iteraciones, pos;
	float Maximo=-FLT_MAX;


	iteraciones=(int)(GENERACIONES);

	// Crear BEM_Elipses soluci�n
	Mejor=Crear_BEM_Elipses(Problema);


	//
	// AGG
	//

	// Crear ecosistema
	G1=Crear_Generacion_BEM_Elipse(POBLACION);
	G2=Crear_Generacion_BEM_Elipse(POBLACION);

	// Crear primera generaci�n
	InicGeneracion_BEM_Elipse(G1, Problema, &Seed);


	//
	// Estas son las soluciones reales
	//
	/********************************* 0.06875 0.025 -0.0225 0.0046875  35.0 */
	
	/*G1->Poblacion[0]->Elipse[1].V[0]=0.06875;
	G1->Poblacion[0]->Elipse[1].V[1]=0.025;
	G1->Poblacion[0]->Elipse[1].V[2]=35.0*PI/180;
	G1->Poblacion[0]->Elipse[1].V[3]=0.0225;
	G1->Poblacion[0]->Elipse[1].V[4]=0.0046875;*/

	/********************************* 0.05 0.075  -0.01875 .009375 135.0 */

	/*G1->Poblacion[0]->Elipse[2].V[0]=0.05;
	G1->Poblacion[0]->Elipse[2].V[1]=0.075;
	G1->Poblacion[0]->Elipse[2].V[2]=135.0*PI/180;
	G1->Poblacion[0]->Elipse[2].V[3]=0.01875;
	G1->Poblacion[0]->Elipse[2].V[4]=0.009375;*/

	/********************************* 0.02 0.025  -0.01 */

	/*G1->Poblacion[0]->Elipse[0].V[0]=0.02;
	G1->Poblacion[0]->Elipse[0].V[1]=0.025;
	G1->Poblacion[0]->Elipse[0].V[2]=0;
	G1->Poblacion[0]->Elipse[0].V[3]=0.01;
	G1->Poblacion[0]->Elipse[0].V[4]=0.01;*/

	/**********************************/


	// Evaluar generaci�n
	EvGeneracion_BEM_Elipse(G1);


	// ITERACIONES
	for(i=0; i<iteraciones; i++)
	{
		// Verbose
		if(!(i%4))
		{		
			// Buscar el mejor cromosoma
			Maximo=-FLT_MAX;
			for(j=0; j<G1->Tam; j++)
			{
				if(G1->Poblacion[j]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[j]->CostoActual;
					pos=j;
				}
			}

			printf("%d %f\n", i, G1->Poblacion[pos]->CostoActual);
			Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);
		}

		// Selecci�n de cromosomas
		AsignarPs_BEM_Elipse(G1);
		SeleccionBaker_BEM_Elipse(G1, &Seed, POBLACION);

		// Siguiente generaci�n
		for(pos=0, j=0; j<G1->Tam; j++)
		{
			while(G1->Selec[j])
			{
				G2->Poblacion[pos]=Crear_BEM_Elipses(Problema);
				Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
				pos++;
				G1->Selec[j]--;
			}
		}
		
		// Cruce
		AGG_Cruce_BEM_Elipse(G2, &Seed);

		// Mutaci�n
		for(j=0; j<G2->Tam; j++)
		{
			if(Randfloat(0, 1)<MUTACION)
			{
				AG_Mutacion_BEM_Elipse(G2->Poblacion[j], &Seed);
				AG_Mutacion2_BEM_Elipse(G2->Poblacion[j], &Seed);
			}
		}

		// Evaluar
		EvGeneracion_BEM_Elipse(G2);

		// Envejecimiento de la poblaci�n con elitismo
		SustGeneracion_BEM_Elipse(G1, G2);
		
		/*// Exterminar generaci�n anterior
		Exterminar_Generacion_BEM_Elipse(G1);
		// Envejecimiento de la poblaci�n
		for(j=0; j<G2->Tam; j++) G1->Poblacion[j]=G2->Poblacion[j];*/
	}


	// Buscar el mejor cromosoma
	Maximo=-FLT_MAX;
	for(i=0; i<G1->Tam; i++)
	{
		if(G1->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G1->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo y exterminar al resto
	Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
	Exterminar_Generacion_BEM_Elipse(G1);

	// Liberar memoria
	Destruir_Generacion_BEM_Elipse(G1);
	Destruir_Generacion_BEM_Elipse(G2);

	return Mejor;
}


// Algoritmo Gen�tico Generacional Longitur Variable para el problema BEM_Elipse
BEM_Elipses AGGLV_BEM_Elipse(BEM_Placa Problema, char * f_graficos, 
						   int Seed)
{
	Generacion_BEM_Elipse G1, G2;
	BEM_Elipses Mejor;
	int i, j, iteraciones, pos;
	float Maximo=-FLT_MAX, Media, Desviacion;


	iteraciones=(int)(GENERACIONES);

	// Crear BEM_Elipses soluci�n
	Mejor=Crear_BEM_Elipses(Problema);


	//
	// AGG
	//

	// Crear ecosistema
	G1=Crear_Generacion_BEM_Elipse(POBLACION);
	G2=Crear_Generacion_BEM_Elipse(POBLACION);

	// Crear primera generaci�n
	InicGeneracion_BEM_Elipse_LV(G1, Problema, &Seed);

	// Evaluar generaci�n
	EvGeneracion_BEM_Elipse(G1);


	// ITERACIONES
	for(i=0; i<iteraciones; i++)
	{
		// Verbose
		if(!(i%10))
		{		
			// Buscar el mejor cromosoma
			Maximo=-FLT_MAX;
			for(j=0; j<G1->Tam; j++)
			{
				if(G1->Poblacion[j]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[j]->CostoActual;
					pos=j;
				}
			}

			Estadisticas_Generacion_BEM_Elipse(G1, &Media, &Desviacion);
			printf("%d %f %f %f\n", i, G1->Poblacion[pos]->CostoActual, Media, Desviacion);
			Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);

			// Si se supera el Umbral, no continuar
			if(G1->Poblacion[pos]->CostoActual>UMBRAL_FITNESS)
				i=iteraciones;
		}

		// Selecci�n de cromosomas
		SeleccionTorneo_BEM_Elipse(G1, &Seed);

		// Reordenar c�digo gen�tico
		for(j=0; j<G1->Tam; j++)
			ReordenarCodigo_BEM_Elipses(G1->Poblacion[j]);

		// Siguiente generaci�n
		for(pos=0, j=0; j<G1->Tam; j++)
		{
			while(G1->Selec[j])
			{
				G2->Poblacion[pos]=Crear_BEM_Elipses(Problema);
				Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
				pos++;
				G1->Selec[j]--;
			}
		}
		
		// Cruce
		AGG_Cruce_BEM_Elipse(G2, &Seed);

		// Mutaci�n
		for(j=0; j<G2->Tam; j++)
		{
			if(Randfloat(0, 1)<MUTA_LONGITUD)
				AG_MutaLongitud_BEM_Elipse(G2->Poblacion[j], &Seed);

			if(Randfloat(0, 1)<MUTACION)
			{
				AG_Mutacion_BEM_Elipse(G2->Poblacion[j], &Seed);
				AG_Mutacion2_BEM_Elipse(G2->Poblacion[j], &Seed);
			}
		}

		// Evaluar
		EvGeneracion_BEM_Elipse(G2);

		// Envejecimiento de la poblaci�n con elitismo
		SustGeneracion2_BEM_Elipse(G1, G2);
	}


	// Buscar el mejor cromosoma
	Maximo=-FLT_MAX;
	for(i=0; i<G1->Tam; i++)
	{
		if(G1->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G1->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo y exterminar al resto
	Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
	Exterminar_Generacion_BEM_Elipse(G1);

	// Liberar memoria
	Destruir_Generacion_BEM_Elipse(G1);
	Destruir_Generacion_BEM_Elipse(G2);

	return Mejor;
}



// Algoritmo Gen�tico CHC para el problema BEM_Elipse
BEM_Elipses AGCHC_BEM_Elipse(BEM_Placa Problema, char * f_graficos,
							 int Seed)
{
	Generacion_BEM_Elipse G1, G2;
	BEM_Elipses Mejor;
	int i, j, iteraciones, pos, Umbral;
	float Maximo=-FLT_MAX;


	iteraciones=(int)(ITERACIONES);

	// Crear Clas soluci�n
	Mejor=Crear_BEM_Elipses(Problema);


	//
	// CHC
	//

	// Crear ecosistema
	G1=Crear_Generacion_BEM_Elipse(POBLACION);
	G2=Crear_Generacion_BEM_Elipse(POBLACION);

	// Crear primera generaci�n
	InicGeneracion_BEM_Elipse(G1, Problema, &Seed);

	// Evaluar generaci�n
	EvGeneracion_BEM_Elipse(G1);
	i=G1->Tam;

	// Inicializar umbral de cruce
	Umbral=G1->Poblacion[0]->Tam*G1->Poblacion[0]->Elipse[0].NParams/4;


	// ITERACIONES
	while(i<iteraciones)
	{
		// Verbose
		if(!(i%(G1->Tam*4)))
		{		
			// Buscar el mejor cromosoma
			Maximo=-FLT_MAX;
			for(j=0; j<G1->Tam; j++)
			{
				if(G1->Poblacion[j]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[j]->CostoActual;
					pos=j;
				}
			}

			printf("%d %f\n", i, G1->Poblacion[pos]->CostoActual);
			Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);
		}


		// Reinicializar
		if(Umbral<0) // NBITS?
		{
			ReiniciarGeneracion_BEM_Elipse(G1, &Seed);
			EvGeneracion_BEM_Elipse(G1);
			i+=50;
			Umbral=G1->Poblacion[0]->Tam*G1->Poblacion[0]->Elipse[0].NParams/4;
		}

		// Selecci�n de cromosomas
		SeleccionTorneo_BEM_Elipse(G1, &Seed);

		// Siguiente generaci�n
		for(pos=0, j=0; j<G1->Tam; j++)
		{
			while(G1->Selec[j])
			{
				G2->Poblacion[pos]=Crear_BEM_Elipses(Problema);
				Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
				pos++;
				G1->Selec[j]--;
			}
		}

		// Reordenar c�digo gen�tico
		for(j=0; j<G1->Tam; j++)
			ReordenarCodigo_BEM_Elipses(G1->Poblacion[j]);

		
		// Cruce
		AGCHC_Cruce_BEM_Elipse(G2, &Umbral, &Seed);

		// Evaluar
		EvGeneracion_BEM_Elipse(G2);
		i+=G2->Tam;

		// Envejecimiento de la poblaci�n con elitismo
		SustGeneracion_BEM_Elipse(G1, G2);
	}


	// Buscar el mejor cromosoma
	Maximo=-FLT_MAX;
	for(i=0; i<G1->Tam; i++)
	{
		if(G1->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G1->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo y exterminar al resto
	Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
	Exterminar_Generacion_BEM_Elipse(G1);

	// Liberar memoria
	Destruir_Generacion_BEM_Elipse(G1);
	Destruir_Generacion_BEM_Elipse(G2);

	return Mejor;
}


// Estrategia de Evoluci�n 1,1 aplicada sobre el problema utilizando elipses
BEM_Elipses Ee1_BEM_Elipse(BEM_Placa Problema, char * f_graficos, 
						   int Seed)
{
	Generacion_BEM_Elipse G1, G2;
	BEM_Elipses Mejor;
	int i, j, iteraciones, pos, n=10;
	float Maximo=-FLT_MAX, varianza=0.25, c=0.85, p;
	int N_padres=1, N_hijos=1, n_mutaciones, sin_exito, n_exitos;
	int it_adaptacion, adaptar, n_variables, num_gen_ee=500, nuevo=0;

	printf("\nEstrategia de Evolucion 1+1, con tope de iteraciones sin exito: %d\n\n", num_gen_ee);

	// Inicializar valores
	iteraciones=(int)(GENERACIONES);

	// Crear BEM_Elipses soluci�n
	Mejor=Crear_BEM_Elipses(Problema);

	// Crear ecosistema
	G1=Crear_Generacion_BEM_Elipse(N_padres);
	G2=Crear_Generacion_BEM_Elipse(N_hijos);

	// Crear primera generaci�n
	InicGeneracion_BEM_Elipse(G1, Problema, &Seed);	// Valores aleatorios

	// Evaluar generaci�n
	EvGeneracion_BEM_Elipse(G1);

	n_variables=G1->Poblacion[0]->Elipse[0].NParams * N_ELIPSES;
	n_mutaciones=n_exitos=sin_exito=0;
	it_adaptacion=20 * n_variables;
	adaptar=0;

	// ITERACIONES
	//for(i=0; i<iteraciones; i++)
	i=0;
	do
	{

		// Verbose
		//if(!(i%8)&&(i!=0)&&(nuevo!=0))
		if((i!=0)&&(nuevo!=0))
		{
			nuevo=0;

			// Buscar el mejor cromosoma para imprimirlo
			Maximo=-FLT_MAX;
			for(j=0; j<G1->Tam; j++)
			{
				if(G1->Poblacion[j]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[j]->CostoActual;
					pos=j;
				}
			}

			printf("%d %f\n", i, G1->Poblacion[pos]->CostoActual);
			Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);
		}

		// Siguiente generaci�n
		for(pos=0, j=0; j<G1->Tam; j++)
		{
			G2->Poblacion[pos]=Crear_BEM_Elipses(Problema);
			Copia_BEM_Elipses(G1->Poblacion[j], G2->Poblacion[pos]);
			pos++;
		}
		
		// Mutaci�n para todos los hijos
		for(j=0; j<G2->Tam; j++)
		{
			Ee_Mutacion_BEM_Elipse(G2->Poblacion[j], &Seed, varianza);
		}

		// Evaluar
		EvGeneracion_BEM_Elipse(G2);

		// Se contabiliza la nueva mutacion
		n_mutaciones+=1;

		// Seleccionar el mejor entre padres e hijos
		for(j=0; j<G2->Tam; j++)
		{
			if(G2->Poblacion[j]->CostoActual > (G1->Poblacion[j]->CostoActual + 0.05))
			{
				Copia_BEM_Elipses(G2->Poblacion[j], G1->Poblacion[j]);
				n_exitos++;
				sin_exito=0;
				nuevo=1;
			} else
				sin_exito++;
		}

		// A partir del momento en que se hayan generado 10*n_variables
		// descendientes, se adapta sigma
		if (n_mutaciones==it_adaptacion)
		{
			adaptar=1;
			p=n_exitos/(float)n_mutaciones;

			// Si p<1/5, se disminuye sigma (c<1 -> sigma*c^(1/n)<sigma)
			if (p<0.2)
				varianza*=pow (c,1.0/n_variables);

			// Si p>1/5, se aumenta sigma (c<1 -> sigma/c^(1/n)>sigma)
			if (p>0.2)
				varianza/=pow (c,1.0/n_variables);
		}
   
		if ((adaptar) && (n_mutaciones%n_variables==0))
		{
			p=n_exitos/(float)n_mutaciones;

			// Si p<1/5, se disminuye sigma (c<1 -> sigma*c^(1/n)<sigma)
			if (p<0.2)
				varianza*=pow (c,1.0/n_variables);

			// Si p>1/5, se aumenta sigma (c<1 -> sigma/c^(1/n)>sigma)
			if (p>0.2)
				varianza/=pow (c,1.0/n_variables);
		}

		i++;
	} while (sin_exito<num_gen_ee);

	// Buscar el mejor cromosoma
	Maximo=-FLT_MAX;
	for(i=0; i<G1->Tam; i++)
	{
		if(G1->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G1->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo y exterminar al resto
	Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
	Exterminar_Generacion_BEM_Elipse(G1);

	// Liberar memoria
	Destruir_Generacion_BEM_Elipse(G1);
	Destruir_Generacion_BEM_Elipse(G2);

	return Mejor;
}


// Estrategia de Evoluci�n 15,105 aplicada sobre el problema utilizando elipses
BEM_Elipses Ee2_BEM_Elipse(BEM_Placa Problema, char * f_graficos, 
						   int Seed)
{
	Generacion_BEM_Elipse G1, G2;
	BEM_Elipses Mejor;
	int i, j, iteraciones, pos, n=10;
	float Maximo=-FLT_MAX, varianza[105], c=0.85, p;
	float z0, zi, u1, u2;
	int N_padres=15, N_hijos=105, n_mutaciones, n_exitos, it_adaptacion, adaptar, n_variables;
	time_t inicio, fin;

	printf("\nEstrategia de Evolucion mu+lambda, con semilla: %d\n\n", Seed);

	iteraciones=(int)(GENERACIONES);

	// Crear BEM_Elipses soluci�n
	Mejor=Crear_BEM_Elipses(Problema);

	// Crear ecosistema
	G1=Crear_Generacion_BEM_Elipse(N_padres);
	G2=Crear_Generacion_BEM_Elipse(N_hijos);

	// Crear primera generaci�n
	InicGeneracion_BEM_Elipse(G1, Problema, &Seed);	// Valores aleatorios
	InicGeneracion_BEM_Elipse(G2, Problema, &Seed);

	// Evaluar generaci�n
	EvGeneracion_BEM_Elipse(G1);

	// Inicializar varianzas y condici�n de final
	for(i=0; i<N_hijos; i++)
		varianza[i]=0.25;

	time(&inicio);

	i=0;

	// ITERACIONES
	//for(i=0; i<iteraciones; i++)
	do
	{
		// Verbose
		//if(!(i%8))
		//{		
			// Buscar el mejor cromosoma
			Maximo=-FLT_MAX;
			for(j=0; j<G1->Tam; j++)
			{
				if(G1->Poblacion[j]->CostoActual > Maximo)
				{
					Maximo=G1->Poblacion[j]->CostoActual;
					pos=j;
				}
			}

			printf("%d %f\n", i, G1->Poblacion[pos]->CostoActual);
			Imprimir_BEM_Elipses(G1->Poblacion[pos], f_graficos);
		//}

		// Siguiente generaci�n
		Ee_Cruce_BEM_Elipse(G1, G2, &Seed);

		// Obtener z0 para mutar las varianzas
		u1=Rand ();
		u2=Rand ();
		z0 = ((pow((1/sqrt(2*n)),2)) * sqrt (-2*log(u1)) * sin (2*PI*u2));

		// Mutaci�n para todos los hijos
		for(j=0; j<G2->Tam; j++)
		{
			// Mutar las varianzas, autoadaptaci�n de par�metros
			u1=Rand ();
			u2=Rand ();
			zi = ((pow((1/sqrt(2*sqrt(n))),2)) * sqrt (-2*log(u1)) * sin (2*PI*u2));
			varianza[j]=varianza[j]*exp(zi+z0);

			// Mutar cada cromosoma
			Ee_Mutacion_BEM_Elipse(G2->Poblacion[j], &Seed, varianza[j]);
		}

		// Evaluar
		EvGeneracion_BEM_Elipse(G2);

		// Selecci�n de los padres de la siguiente poblaci�n
		Ee_Seleccion_BEM_Elipse(G1, G2);

		// Evaluar
		EvGeneracion_BEM_Elipse(G1);

		time(&fin);
		i++;
	} while (difftime(fin,inicio) < 1800);

	// Buscar el mejor cromosoma
	Maximo=-FLT_MAX;
	for(i=0; i<G1->Tam; i++)
	{
		if(G1->Poblacion[i]->CostoActual > Maximo)
		{
			Maximo=G1->Poblacion[i]->CostoActual;
			pos=i;
		}
	}

	// Copiarlo y exterminar al resto
	Copia_BEM_Elipses(G1->Poblacion[pos], Mejor);
	Exterminar_Generacion_BEM_Elipse(G1);

	// Liberar memoria
	Destruir_Generacion_BEM_Elipse(G1);
	Destruir_Generacion_BEM_Elipse(G2);

	return Mejor;
}



//
// Operadores Gen�ticos
//


// Operador Cruce
void AG_Cruce_BEM_Elipse(BEM_Elipses Padre, BEM_Elipses Madre, 
						 BEM_Elipses Hijo1, BEM_Elipses Hijo2, 
						 int * Seed1)
{
	int Seed=*Seed1, valor, i, j;

	// Copiar Padres en los Hijos
	Copia_BEM_Elipses(Padre, Hijo1);
	Copia_BEM_Elipses(Madre, Hijo2);


	// Cruce simple

	// Elegir punto de cruce
	valor=Randint(0, Padre->Tam-1);

	// Cruzar
	// Para todas las elipses
	for(i=0; i<valor; i++)
	{
		// Para todos los genes
		for(j=0; j<Padre->Elipse[0].NParams; j++)
			Hijo1->Elipse[i].V[j]=Madre->Elipse[i].V[j];

		if(i<Hijo2->Tam)
		{
			// Para todos los genes
			for(j=0; j<Padre->Elipse[0].NParams; j++)
				Hijo2->Elipse[i].V[j]=Padre->Elipse[i].V[j];
		}
	}

	*Seed1=Seed;
}




// Operador Mutaci�n
void AG_Mutacion_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1)
{
	int Seed=*Seed1, i, j;

	// Para todas las elipses
	for(i=0; i<Antiguo->Tam; i++)
	{
		// Para todos los par�metros
		for(j=0; j<Antiguo->Elipse[0].NParams; j++)
		{
			// Ratio de desplazamiento
			if(Randfloat(0,1) < RD)
			{
				// Incremento
				if(Randfloat(0,1) < 0.5)
					Antiguo->Elipse[i].V[j]+=(float)FD*(Antiguo->Lim[2*j+1] 
						- Antiguo->Lim[2*j]);
				// Disminuci�n
				else
					Antiguo->Elipse[i].V[j]-=(float)FD*(Antiguo->Lim[2*j+1]
						- Antiguo->Lim[2*j]);
			
				// Ajuste
				if(Antiguo->Elipse[i].V[j] < Antiguo->Lim[2*j])
					Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j];
				if(Antiguo->Elipse[i].V[j] > Antiguo->Lim[2*j+1])
					Antiguo->Elipse[i].V[j]=Antiguo->Lim[2*j+1];
			}
		}
	}

	*Seed1=Seed;
}




// Operador Mutaci�n de Longitud de cromosoma
void AG_MutaLongitud_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1)
{
	int Seed=*Seed1, i, j;

	// Incremento
	if(Randfloat(0, 1)<0.5)
	{
		if(Antiguo->Tam<MAXELIPSES)
		{
			i=Antiguo->Tam;
			Antiguo->Tam++;

			// Asignar valores a la elipse nueva
			for(j=0; j<Antiguo->Elipse[i].NParams; j++)
				Antiguo->Elipse[i].V[j]=(float)Randfloat(Antiguo->Lim[j*2], Antiguo->Lim[2*j+1]);
		}
	}
	// Disminuci�n
	else
	{
		if(Antiguo->Tam>0)
		{
			i=Antiguo->Tam-1;
			Antiguo->Tam--;

			// Asignar 0 a la elipse eliminada
			for(j=0; j<Antiguo->Elipse[i].NParams; j++)
				Antiguo->Elipse[i].V[j]=(float)0;
		}
	}

	*Seed1=Seed;
}




// Operador Mutaci�n Aleatorio Simple
void AG_Mutacion2_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1)
{
	int Seed=*Seed1, Elip, Gen;
	float MinV, MaxV;

	// Seleccionar Elipse
	Elip=Randint(0, Antiguo->Tam-1);

	// Seleccionar gen
	Gen=Randint(0, Antiguo->Elipse[0].NParams-1);

	if(Gen!=(Antiguo->Elipse[0].NParams-1))
	{
		MinV=Antiguo->Lim[2*Gen];
		MaxV=Antiguo->Lim[2*Gen+1];
		Antiguo->Elipse[Elip].V[Gen]=(float)(MinV+Randfloat(0,1)*(MaxV-MinV));
	}

	*Seed1=Seed;
}


// Operador Mutaci�n para Estrategias de Evoluci�n
void Ee_Mutacion_BEM_Elipse(BEM_Elipses Antiguo, int * Seed1, float varianza)
{
	int Seed=*Seed1, i, j;
	float normal, MinV, MaxV, u1, u2, nuevo_valor;

	// Para todas las elipses
	for(i=0; i<Antiguo->Tam; i++)
	{
		// Para todos los par�metros
		for(j=0; j<Antiguo->Elipse[0].NParams; j++)
		{
			// Se calculan los l�mites
			MinV=Antiguo->Lim[2*j];
			MaxV=Antiguo->Lim[2*j+1];

			// Distribuci�n Normal de media 0 y varianza
			// Se generan dos uniformes [0,1]
			u1=Rand ();
			u2=Rand ();

			normal = (varianza * sqrt (-2*log(u1)) * sin (2*PI*u2));

			//nuevo_valor = Antiguo->Elipse[i].V[j] + normal;
			nuevo_valor = Antiguo->Elipse[i].V[j] + (float)normal*(MaxV-MinV);

			// Ajuste
			if(nuevo_valor < MinV) {
				Antiguo->Elipse[i].V[j]=MinV;
			} else {
				if(nuevo_valor > MaxV) {
					Antiguo->Elipse[i].V[j]=MaxV;
				} else {
					Antiguo->Elipse[i].V[j]=nuevo_valor;
				}
			}
		}
	}

	*Seed1=Seed;
}


// Realizar una sustituci�n de cromosomas con elitismo
void SustGeneracion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2)
{
	int i, j, *Pos, Maxpos;
	float Max;

	// Reservar memoria
	Pos=(int*)malloc(sizeof(int)*G1->Tam*2);
	if(Pos==NULL)
		Error("sin memoria para Pos");

	// Inicializar cromosomas elegidos
	for(i=0; i<G1->Tam*2; i++)
		Pos[i]=0;

	// Elegir los N mejores
	for(i=0; i<G1->Tam; i++)
	{
		Max=-FLT_MAX;
		Maxpos=-1;

		for(j=0; j<G1->Tam*2; j++)
		{
			// Si no est� seleccionado
			if(!Pos[j])
			{
				// Buscar en generaci�n anterior
				if(j<G1->Tam)
				{
					if(G1->Poblacion[j]->CostoActual>Max)
					{
						Max=G1->Poblacion[j]->CostoActual;
						Maxpos=j;
					}
				}
				// Buscar en generaci�n nueva
				else
				{
					if(G2->Poblacion[j-G1->Tam]->CostoActual>Max)
					{
						Max=G2->Poblacion[j-G1->Tam]->CostoActual;
						Maxpos=j;
					}
				}
			}
		}

		// Seleccionar
		Pos[Maxpos]++;
	}

	// Eliminar los no elegidos
	for(i=0; i<G1->Tam*2; i++)
	{
		if(!Pos[i])
		{
			if(i<G1->Tam)
				Destruir_BEM_Elipses(G1->Poblacion[i]);
			else
				Destruir_BEM_Elipses(G2->Poblacion[i-G1->Tam]);
		}
	}

	// Salvar los seleccionados
	j=0;
	for(i=0; i<G1->Tam*2; i++)
	{
		if(Pos[i])
		{
			if(i<G1->Tam)
				G1->Poblacion[j]=G1->Poblacion[i];
			else
				G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
			j++;
		}
	}

	// Liberar memoria
	free(Pos);
}


// Realizar una sustituci�n de cromosomas con elitismo variable
void SustGeneracion2_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2)
{
	int i, j, *Pos, Maxpos, Minpos;
	float Max, Min;

	// Reservar memoria
	Pos=(int*)malloc(sizeof(int)*G1->Tam*2);
	if(Pos==NULL)
		Error("sin memoria para Pos");

	// Inicializar cromosomas elegidos en G1
	for(i=0; i<G1->Tam; i++)
		Pos[i]=0;

	// Inicializar cromosomas elegidos en G2
	for(i=G2->Tam; i<G2->Tam*2; i++)
		Pos[i]=1;

	// Elegir los N mejores de G1
	for(i=0; i<ELITISMO; i++)
	{
		Max=-FLT_MAX;
		Maxpos=-1;

		for(j=0; j<G1->Tam; j++)
		{
			// Si no est� seleccionado
			if(!Pos[j])
			{
				// Buscar en generaci�n anterior
				if(G1->Poblacion[j]->CostoActual>Max)
				{
					Max=G1->Poblacion[j]->CostoActual;
					Maxpos=j;
				}
			}
		}

		// Seleccionar
		Pos[Maxpos]++;
	}

	// Elegir los N peores de G2
	for(i=0; i<ELITISMO; i++)
	{
		Min=FLT_MAX;
		Minpos=-1;

		for(j=G2->Tam; j<G2->Tam*2; j++)
		{
			// Si no est� seleccionado
			if(!Pos[j])
			{
				// Buscar en generaci�n anterior
				if(G2->Poblacion[j-G2->Tam]->CostoActual<Min)
				{
					Min=G2->Poblacion[j-G2->Tam]->CostoActual;
					Minpos=j;
				}
			}
		}

		// Seleccionar
		Pos[Minpos]--;
	}

	// Eliminar los no elegidos de G1
	for(i=0; i<G1->Tam; i++)
	{
		if(!Pos[i])
			Destruir_BEM_Elipses(G1->Poblacion[i]);
	}

	// Eliminar los no elegidos de G2
	for(i=G2->Tam; i<G2->Tam*2; i++)
	{
		if(!Pos[i])
			Destruir_BEM_Elipses(G2->Poblacion[i-G2->Tam]);
	}

	// Salvar los seleccionados
	j=0;
	for(i=0; i<G1->Tam*2; i++)
	{
		if(Pos[i])
		{
			if(i<G1->Tam)
				G1->Poblacion[j]=G1->Poblacion[i];
			else
				G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
			j++;
		}
	}

	// Liberar memoria
	//free(Pos);
}



// Cruce generacional en Generacion_BEM_Elipse
void AGG_Cruce_BEM_Elipse(Generacion_BEM_Elipse G, int *Seed1)
{
	BEM_Elipses aux, Hijo1, Hijo2;
	int Seed=*Seed1;
	int i, pos, iteraciones;

	iteraciones=(int)(G->Tam*CRUCE);

	// Barajar
	for(i=0; i<G->Tam; i++)
	{
		pos=Randint(0, G->Tam-1);
		aux=G->Poblacion[i];
		G->Poblacion[i]=G->Poblacion[pos];
		G->Poblacion[pos]=aux;
	}
		
	// Cruzar
	for(i=0; i<iteraciones; i+=2)
	{
		Hijo1=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		Hijo2=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		
		AG_Cruce_BEM_Elipse(G->Poblacion[i], G->Poblacion[i+1], 
		Hijo1, Hijo2, &Seed);
		
		Destruir_BEM_Elipses(G->Poblacion[i]);
		Destruir_BEM_Elipses(G->Poblacion[i+1]);
		G->Poblacion[i]=Hijo1;
		G->Poblacion[i+1]=Hijo2;
	}

	*Seed1=Seed;
}


// Cruce para Estrategias de Evoluci�n
void Ee_Cruce_BEM_Elipse(Generacion_BEM_Elipse G1, Generacion_BEM_Elipse G2, int *Seed1)
{
	int Seed=*Seed1;
	int i, j, k, pos, c;

	// Obtener C, el numero de cromosomas a recombinar
	c=Randint(0, G1->Tam-1);

	// Cruzar
	for(i=0; i<G2->Tam; i++)
	{
		// Para todas las elipses
		for(j=0; j<G2->Poblacion[i]->Tam; j++)
		{
			// Para todos los genes
			for(k=0; k<G2->Poblacion[i]->Elipse[0].NParams; k++)
			{
				pos=Randint(0,c);
				G2->Poblacion[i]->Elipse[j].V[k]=G1->Poblacion[pos]->Elipse[j].V[k];
			}
		}
		
	}

	*Seed1=Seed;
}


// Cruce generacional en Generacion_BEM_Elipse
void AGCHC_Cruce_BEM_Elipse(Generacion_BEM_Elipse G, int * Umbral, 
							int *Seed1)
{
	BEM_Elipses aux, Hijo1, Hijo2;
	int Seed=*Seed1;
	int i, Cruces, pos, iteraciones;

	iteraciones=(int)(G->Tam*CRUCE);

	// Barajar
	for(i=0; i<G->Tam; i++)
	{
		pos=Randint(0, G->Tam-1);
		aux=G->Poblacion[i];
		G->Poblacion[i]=G->Poblacion[pos];
		G->Poblacion[pos]=aux;
	}
		
	Cruces=0;

	// Cruzar
	for(i=0; i<iteraciones; i+=2)
	{
		// Si difieren lo suficiente
		if((Distancia_BEM_Elipses(G->Poblacion[i], G->Poblacion[i+1])/(2*NBITS))
			>=(*Umbral))
		{
			Cruces++;

			Hijo1=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
			Hijo2=Crear_BEM_Elipses(G->Poblacion[0]->Problema);
		
			AG_Cruce_BEM_Elipse(G->Poblacion[i], G->Poblacion[i+1], 
			Hijo1, Hijo2, &Seed);
			
			Destruir_BEM_Elipses(G->Poblacion[i]);
			Destruir_BEM_Elipses(G->Poblacion[i+1]);
			G->Poblacion[i]=Hijo1;
			G->Poblacion[i+1]=Hijo2;
		}
	}

	// Si no ha habido cruces
	if(!Cruces)
		(*Umbral)--;

	*Seed1=Seed;
}


// Realizar una selecci�n de padres para la siguiente poblaci�n
void Seleccion_Generacion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2)
{
	int i, j, *Pos, Maxpos, tam;
	float Max;

	// Individuos a estudiar
	tam = G1->Tam + G2->Tam;

	// Reservar memoria
	Pos=(int*)malloc(sizeof(int)*tam);
	if(Pos==NULL)
		Error("sin memoria para Pos, en selecci�n de hijos");

	// Inicializar cromosomas elegidos
	for(i=0; i<tam; i++)
		Pos[i]=0;

	// Elegir los N mejores
	for(i=0; i<G1->Tam; i++)
	{
		Max=-FLT_MAX;
		Maxpos=-1;

		for(j=0; j<tam; j++)
		{
			// Si no est� seleccionado
			if(!Pos[j])
			{
				// Buscar en generaci�n anterior
				if(j<G1->Tam)
				{
					if(G1->Poblacion[j]->CostoActual>Max)
					{
						Max=G1->Poblacion[j]->CostoActual;
						Maxpos=j;
					}
				}
				// Buscar en generaci�n nueva
				else
				{
					if(G2->Poblacion[j-G1->Tam]->CostoActual>Max)
					{
						Max=G2->Poblacion[j-G1->Tam]->CostoActual;
						Maxpos=j;
					}
				}
			}
		}

		// Seleccionar
		Pos[Maxpos]++;
	}

	// Eliminar los no elegidos
	for(i=0; i<tam; i++)
	{
		if(!Pos[i])
		{
			if(i<G1->Tam)
				Destruir_BEM_Elipses(G1->Poblacion[i]);
			else
				Destruir_BEM_Elipses(G2->Poblacion[i-G1->Tam]);
		}
	}

	// Salvar los seleccionados
	j=0;
	for(i=0; i<tam; i++)
	{
		if(Pos[i])
		{
			if(i<G1->Tam)
				G1->Poblacion[j]=G1->Poblacion[i];
			else
				G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
			j++;
		}
	}

	// Liberar memoria
	free(Pos);
}


// Realizar una selecci�n de padres en Estrategias de Evoluci�n
void Ee_Seleccion_BEM_Elipse(Generacion_BEM_Elipse G1, 
							   Generacion_BEM_Elipse G2)
{
	BEM_Elipses aux;
	int i, j, *Pos, Maxpos, tam;
	float Max;

	// Individuos a estudiar
	tam = G1->Tam + G2->Tam;

	// Reservar memoria
	Pos=(int*)malloc(sizeof(int)*tam);
	if(Pos==NULL)
		Error("sin memoria para Pos, en selecci�n de hijos");

	// Inicializar cromosomas elegidos
	for(i=0; i<tam; i++)
		Pos[i]=0;

	// Elegir los N mejores
	for(i=0; i<G1->Tam; i++)
	{
		Max=-FLT_MAX;
		Maxpos=-1;

		for(j=0; j<tam; j++)
		{
			// Si no est� seleccionado
			if(!Pos[j])
			{
				// Buscar en generaci�n anterior
				if(j<G1->Tam)
				{
					if(G1->Poblacion[j]->CostoActual>Max)
					{
						Max=G1->Poblacion[j]->CostoActual;
						Maxpos=j;
					}
				}
				// Buscar en generaci�n nueva
				else
				{
					if(G2->Poblacion[j-G1->Tam]->CostoActual>Max)
					{
						Max=G2->Poblacion[j-G1->Tam]->CostoActual;
						Maxpos=j;
					}
				}
			}
		}

		// Seleccionar
		Pos[Maxpos]++;
	}

	// Salvar los seleccionados
	j=0;
	for(i=0; i<tam; i++)
	{
		if(Pos[i])
		{
			if(i<G1->Tam)
			{
				aux=G1->Poblacion[j];
				G1->Poblacion[j]=G1->Poblacion[i];
				G1->Poblacion[i]=aux;
			} else {
				aux=G1->Poblacion[j];
				G1->Poblacion[j]=G2->Poblacion[i-G1->Tam];
				G2->Poblacion[i-G1->Tam]=aux;
			}
			j++;
		}
	}

	// Liberar memoria
	free(Pos);
}


// Distancia entre BEM_Elipses
int Distancia_BEM_Elipses(BEM_Elipses C1, BEM_Elipses C2)
{
	char *cx, *cy;
	short c1, c2, uno=1;
	int Distancia=0, i, j, k;

	// Para todas las reglas
	for(i=0; i<C1->Tam; i++)
	{
		// Para todos los genes reales
		for(j=0; j<C1->Elipse[0].NParams; j++)
		{
			cx=(char*)&c1;
			cy=(char*)&C1->Elipse[i].V[j];
			*cx=*cy;
			*(cx+1)=*(cy+1);

			cx=(char*)&c2;
			cy=(char*)&C2->Elipse[i].V[j];
			*cx=*cy;
			*(cx+1)=*(cy+1);

			c1=c1 ^ c2;
			
			// Para los NBits primeros bits
			for(k=0; k<NBITS; k++)
			{
				c2=uno<<k;

				if(c1 & c2)
					Distancia++;
			}
		}
	}

	return Distancia;
}


			/********************************/
			/* Problema BEM para Placa      */
			/* Parametrizaci�n El�ptica     */
			/********************************/



// Inclusi�n de bibliotecas
#include<float.h>
#include "../BEM/Funcs_Aux.h"
#include "BEM_Elipse.h"
#include "random.h"




// Creaci�n de un BEM_Placa
BEM_Placa Crear_BEM_Placa()
{
	BEM_Placa Nueva;

	Nueva=(BEM_Placa)malloc(sizeof(TDA_BEM_Placa));
	if(Nueva==NULL)
		Error("sin memoria para BEM_Placa");

	return Nueva;
}



// Destrucci�n de un BEM_Placa
void Destruir_BEM_Placa(BEM_Placa X)
{
	free(X);
}



// Funci�n para leer el problema desde fichero
BEM_Placa Leer_BEM_Placa(char * s)
{
	FILE *f;
	BEM_Placa Nueva;

	// Abrir fichero de lectura
	f=fopen(s, "rb");
	if(f==NULL)
		Error("no se puede leer BEM_Placa");

	// Crear nueva placa
	Nueva=Crear_BEM_Placa();

	// Leer valores y asignarlos
	fscanf(f, "%f %f %f %f %f %f", &Nueva->U1, &Nueva->U2, 
		&Nueva->V1,	&Nueva->V2, &Nueva->Modulo_Poisson,
		&Nueva->Modulo_Rigidez);

	return Nueva;
}




// Creaci�n de un BEM_Elipses
BEM_Elipses Crear_BEM_Elipses(BEM_Placa X)
{
	int i, j;
	BEM_Elipses Nuevo;

	// Reservar memoria
	Nuevo=(BEM_Elipses)malloc(sizeof(TDA_BEM_Elipses));
	if(Nuevo==NULL)
		Error("sin memoria para BEM_Elipses");

	// Inicializar costo
	Nuevo->CostoActual=0.0;

	// Tama�o por defecto = N_ELIPSES
	Nuevo->Tam=N_ELIPSES;
	Nuevo->Problema=X;

	// L�mites // ?
	Nuevo->NLims=10;
	Nuevo->Lim[0]=(float)(X->U1+(X->U2-X->U1)*0.025);
	Nuevo->Lim[1]=(float)(Nuevo->Lim[0]+(X->U2-X->U1)*0.95);
	Nuevo->Lim[2]=(float)(X->V1+(X->V2-X->V1)*0.025);
	Nuevo->Lim[3]=(float)(Nuevo->Lim[2]+(X->V2-X->V1)*0.95);
	Nuevo->Lim[4]=(float)0;
	Nuevo->Lim[5]=(float)PI;
	Nuevo->Lim[6]=(float)(0.025*Min(X->U2-X->U1, X->V2-X->V1));
	// 0.55*Diagonal?
	Nuevo->Lim[7]=(float)(0.4*sqrt(pow(X->U2-X->U1, 2)+pow(X->V2-X->V1, 2)));
	Nuevo->Lim[8]=Nuevo->Lim[6];
	Nuevo->Lim[9]=Nuevo->Lim[7];

	// Inicializar punteros a l�mites
	Nuevo->MinX=&Nuevo->Lim[0];
	Nuevo->MaxX=&Nuevo->Lim[1];
	Nuevo->MinY=&Nuevo->Lim[2];
	Nuevo->MaxY=&Nuevo->Lim[3];
	Nuevo->MinTheta=&Nuevo->Lim[4];
	Nuevo->MaxTheta=&Nuevo->Lim[5];
	Nuevo->MinA=&Nuevo->Lim[6];
	Nuevo->MaxA=&Nuevo->Lim[7];
	Nuevo->MinB=&Nuevo->Lim[8];
	Nuevo->MaxB=&Nuevo->Lim[9];

	// Asignar valores por defecto
	for(i=0; i<MAXELIPSES; i++)
	{
		Nuevo->Elipse[i].NParams=5;

		for(j=0; j<Nuevo->Elipse[i].NParams; j++)
			Nuevo->Elipse[i].V[j]=0;
			
		Nuevo->Elipse[i].X=&(Nuevo->Elipse[i].V[0]);
		Nuevo->Elipse[i].Y=&(Nuevo->Elipse[i].V[1]);
		Nuevo->Elipse[i].Theta=&(Nuevo->Elipse[i].V[2]);
		Nuevo->Elipse[i].A=&(Nuevo->Elipse[i].V[3]);
		Nuevo->Elipse[i].B=&(Nuevo->Elipse[i].V[4]);
	}

	return Nuevo;
}



// Destrucci�n de un BEM_Elipses
void Destruir_BEM_Elipses(BEM_Elipses X)
{
	free(X);
}




//
// Operadores 
//

// Asignaci�n aleatoria de soluci�n BEM_Elipses
void AsignarAleatorio_BEM_Elipses(BEM_Elipses X, int * Seed1)
{
	int i, j, Seed=*Seed1;

	// Para todas las elipses del cromosoma
	for(i=0; i<X->Tam; i++)
	{
		// Generar los par�metros
		for(j=0; j<X->Elipse[i].NParams; j++)
			X->Elipse[i].V[j]=(float)Randfloat(X->Lim[j*2], X->Lim[2*j+1]);
	}

	*Seed1=Seed;
}



// Copia de estructuras BEM_Elipses
void Copia_BEM_Elipses(BEM_Elipses X, BEM_Elipses Y)
{
	int i, j;

	// Copiar tama�o, costo y problema
	Y->Tam=X->Tam;
	Y->CostoActual=X->CostoActual;
	Y->Problema=X->Problema;

	// Para todas las elipses
	for(i=0; i<MAXELIPSES; i++)
	{
		// Copiar todo el contenido
		for(j=0; j<Y->Elipse[i].NParams; j++)
			Y->Elipse[i].V[j]=X->Elipse[i].V[j];
	}
}




// Imprimir BEM_Elipses
void Imprimir_BEM_Elipses(BEM_Elipses X, char * file)
{
	int i, j;
	FILE *f;

	// Abrir fichero
	if(file==NULL)
		f=stdout;
	else
	{
		f=fopen(file, "a");
		if(f==NULL)
			Error("no se puede imprimir Elipse");
	}

	// Imprimir Fitness
	fprintf(f, "%.5f\n", X->CostoActual);

	// Numero de elipses
	fprintf(f, "%d\n", X->Tam);
	
	// Imprimir todas las elipses
	for(i=0; i<X->Tam; i++)
	{
		for(j=0; j<X->Elipse[i].NParams; j++)
			fprintf(f, "%f ", X->Elipse[i].V[j]);

		fprintf(f, "\n");
	}

	// Cerrar fichero
	if(file!=NULL)
		fclose(f);
}




// Validar BEM_Elipses con respecto a los bordes
void ValidarBordes_BEM_Elipses(BEM_Elipses X)
{
	int i, j;
	float Xc, Yc, A, Alfa, Theta, K1, K2, aux;
	float ATg[5], Min;

	// Para todas las elipses
	for(i=0; i<X->Tam; i++)
	{
		// Calcular eje mayor
		if(X->Elipse[i].V[3] < X->Elipse[i].V[4])
		{
			aux=X->Elipse[i].V[4];
			X->Elipse[i].V[4]=X->Elipse[i].V[3];
			X->Elipse[i].V[3]=aux;
		}

		// Cargar par�metros
		Xc=X->Elipse[i].V[0];
		Yc=X->Elipse[i].V[1];
		Theta=X->Elipse[i].V[2];
		A=X->Elipse[i].V[3];
		
		if(A>0)
			Alfa=X->Elipse[i].V[4]/X->Elipse[i].V[3];
		else
			Alfa=1;


		// Calcular parte constante
		aux=Alfa*Alfa;
		K1=(float)(sqrt(2)/sqrt(1+aux+(1-aux)*cos(2*Theta)));
		K2=(float)(sqrt(2)/sqrt(1+aux+(aux-1)*cos(2*Theta)));


		// Calcular A para obtener tangencia en los bordes
		ATg[0]=(Xc-X->Problema->U1)*K1;
		ATg[1]=(X->Problema->U2-Xc)*K1;
		ATg[2]=(Yc-X->Problema->V1)*K2;
		ATg[3]=(X->Problema->V2-Yc)*K2;
		ATg[4]=A;

		// Calcular m�nimo A necesario
		Min=FLT_MAX;
		for(j=0; j<5; j++)
		{
			if(ATg[j]<Min)
				Min=ATg[j];
		}

		// Aplicar nuevo tama�o
		if(Min<A)
			Min*=(float)EPSILON;
		
		X->Elipse[i].V[3]=Min;
		X->Elipse[i].V[4]=Alfa*Min;
	}
}




// Reordenar c�digo gen�tico
void ReordenarCodigo_BEM_Elipses(BEM_Elipses X)
{
	float aux;
	int i, j, k;

	// Ordenar de izquierda a derecha, atendiendo a Xc
	for(i=0; i<X->Tam-1; i++)
	{
		for(j=i+1; j<X->Tam; j++)
		{
			// En caso de empate, se ordena de arriba a bajo
			if((X->Elipse[i].V[0] > X->Elipse[j].V[0]) ||
				(X->Elipse[i].V[0]==X->Elipse[j].V[0] &&
				X->Elipse[i].V[1] > X->Elipse[j].V[1]))
			{
				// Intercambiar
				for(k=0; k<X->Elipse[i].NParams; k++)
				{
					aux=X->Elipse[i].V[k];
					X->Elipse[i].V[k]=X->Elipse[j].V[k];
					X->Elipse[j].V[k]=aux;
				}
			}
		}
	}
}
